<?php

namespace BirdSystem\SDK\Client\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * ClientVatNumber.
 */
class ClientVatNumber extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var int
     */
    public $companyId = null;

    /**
     * @var int
     */
    public $clientId = null;

    /**
     * @var string
     */
    public $name = null;

    /**
     * @var string
     */
    public $vatNumber = null;

    /**
     * @var string
     */
    public $companyName = null;

    /**
     * @var string
     */
    public $companyAddress = null;

    /**
     * @var string|null
     */
    public $companyAddressCn = null;

    /**
     * @var string
     */
    public $countryIso = null;

    /**
     * @var string|null
     */
    public $telephone = null;

    /**
     * @var string|null
     */
    public $note = null;

    /**
     * @var string
     */
    public $updateTime = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     */
    public $availableTime = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     */
    public $status = null;
}
