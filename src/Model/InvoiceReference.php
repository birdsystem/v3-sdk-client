<?php

namespace BirdSystem\SDK\Client\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * InvoiceReference.
 */
class InvoiceReference extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var int
     */
    public $invoiceId = null;

    /**
     * @var string
     */
    public $value = null;

    /**
     * @var string
     */
    public $valueType = null;
}
