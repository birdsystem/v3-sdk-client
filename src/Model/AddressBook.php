<?php

namespace BirdSystem\SDK\Client\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * AddressBook.
 */
class AddressBook extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var int
     */
    public $companyId = null;

    /**
     * @var int|null
     */
    public $clientId = null;

    /**
     * @var string|null
     */
    public $reference = null;

    /**
     * @var string|null
     */
    public $contact = null;

    /**
     * @var string|null
     */
    public $businessName = null;

    /**
     * @var string
     */
    public $countryIso = null;

    /**
     * @var string|null
     */
    public $postCode = null;

    /**
     * @var string|null
     */
    public $county = null;

    /**
     * @var string|null
     */
    public $city = null;

    /**
     * @var string|null
     */
    public $addressLine1 = null;

    /**
     * @var string|null
     */
    public $addressLine2 = null;

    /**
     * @var string|null
     */
    public $addressLine3 = null;

    /**
     * @var string|null
     */
    public $telephone = null;

    /**
     * @var string|null
     */
    public $fax = null;

    /**
     * @var string|null
     */
    public $email = null;

    /**
     * @var bool
     */
    public $isPublic = null;
}
