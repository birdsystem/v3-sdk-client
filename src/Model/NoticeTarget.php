<?php

namespace BirdSystem\SDK\Client\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * NoticeTarget.
 */
class NoticeTarget extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var int
     */
    public $noticeId = null;

    /**
     * @var int
     */
    public $companyId = null;

    /**
     * @var int|null
     */
    public $clientGroupId = null;

    /**
     * @var int|null
     */
    public $clientId = null;
}
