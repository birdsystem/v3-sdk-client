<?php

namespace BirdSystem\SDK\Client\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * ClientAdditional.
 */
class ClientAdditional extends AbstractModel
{
    /**
     * @var int
     */
    public $clientId = null;

    /**
     * @var int|null
     */
    public $erpPartnerId = null;

    /**
     * @var string|null
     */
    public $amazonAccount = null;

    /**
     * @var string|null
     */
    public $otherAccount = null;

    /**
     * @var string|null
     */
    public $clientName = null;

    /**
     * @var string|null
     */
    public $salesPlatform = null;

    /**
     * @var string|null
     */
    public $majorProduct = null;

    /**
     * @var string|null
     */
    public $exportCountry = null;

    /**
     * @var string|null
     */
    public $numberOfTeamMembers = null;

    /**
     * @var string|null
     */
    public $avgConsignmentCountByMonth = null;

    /**
     * @var string|null
     */
    public $avgDeliveryFeeByMonth = null;

    /**
     * @var string|null
     */
    public $avgConsignmentWeight = null;

    /**
     * @var string|null
     */
    public $packageSizeType = null;

    /**
     * @var string|null
     */
    public $otherOverseaWarehouse = null;

    /**
     * @var string|null
     */
    public $wayKnowBird = null;

    /**
     * @var string|null
     */
    public $services = null;

    /**
     * @var string|null
     */
    public $contractNumber = null;

    /**
     * @var string
     */
    public $trackStatus = null;

    /**
     * @var int|null
     */
    public $trackCompanyUserId = null;

    /**
     * @var string|null
     */
    public $sfAccount = null;

    /**
     * @var string|null
     */
    public $yuntuApiAccount = null;

    /**
     * @var string|null
     */
    public $yuntuApiSecret = null;

    /**
     * @var string|null
     */
    public $anjunApiAccount = null;

    /**
     * @var string|null
     */
    public $anjunApiToken = null;

    /**
     * @var string|null
     */
    public $firstConsignmentTime = null;
}
