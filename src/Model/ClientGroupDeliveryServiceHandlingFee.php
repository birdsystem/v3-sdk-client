<?php

namespace BirdSystem\SDK\Client\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * ClientGroupDeliveryServiceHandlingFee.
 */
class ClientGroupDeliveryServiceHandlingFee extends AbstractModel
{
    /**
     * @var int
     */
    public $clientGroupId = null;

    /**
     * @var int
     */
    public $deliveryServiceId = null;

    /**
     * @var string
     */
    public $handlingFee = '0.00';

    /**
     * @var string
     */
    public $handlingFeeUrgentConsignment = '0.00';

    /**
     * @var bool
     */
    public $isConsignmentSkuHandlingFeeIgnored = null;

    /**
     * @var bool
     */
    public $isUsed = true;
}
