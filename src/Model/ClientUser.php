<?php

namespace BirdSystem\SDK\Client\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * ClientUser.
 */
class ClientUser extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var int|null
     */
    public $companyId = null;

    /**
     * @var int
     */
    public $clientId = null;

    /**
     * @var int|null
     */
    public $clientUserGroupId = null;

    /**
     * @var string
     */
    public $username = null;

    /**
     * @var string
     */
    public $password = null;

    /**
     * @var string|null
     */
    public $apiKey = null;

    /**
     * @var string
     */
    public $apiKeyCrc32 = null;

    /**
     * @var string|null
     */
    public $email = null;

    /**
     * @var string|null
     */
    public $qq = null;

    /**
     * @var string|null
     */
    public $telephone = null;

    /**
     * @var string|null
     */
    public $firstName = null;

    /**
     * @var string|null
     */
    public $lastName = null;

    /**
     * @var string|null
     */
    public $department = null;

    /**
     * @var bool
     */
    public $isReceiveNotificationSms = true;

    /**
     * @var bool
     */
    public $isReceiveNotificationEmail = true;

    /**
     * @var bool
     */
    public $isReceiveNotificationQqGroup = true;

    /**
     * @var int
     */
    public $isChromephpHandlerEnabled = null;

    /**
     * @var string|null
     */
    public $slackAccount = null;

    /**
     * @var string|null
     */
    public $note = null;

    /**
     * @var string
     */
    public $status = null;

    /**
     * @var int|null
     */
    public $timeZoneId = null;

    /**
     * @var string[]
     */
    public $roles = null;

    /**
     * @var string|null
     */
    public $salt = null;
}
