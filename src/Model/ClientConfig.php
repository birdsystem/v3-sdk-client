<?php

namespace BirdSystem\SDK\Client\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * ClientConfig.
 */
class ClientConfig extends AbstractModel
{
    /**
     * @var int
     */
    public $clientId = null;

    /**
     * @var int
     */
    public $companyId = null;

    /**
     * @var string
     */
    public $configCode = null;

    /**
     * @var string|null
     */
    public $value = null;
}
