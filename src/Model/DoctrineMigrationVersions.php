<?php

namespace BirdSystem\SDK\Client\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * DoctrineMigrationVersions.
 */
class DoctrineMigrationVersions extends AbstractModel
{
    /**
     * @var string
     */
    public $version = null;

    /**
     * @var string|null
     */
    public $executedAt = null;

    /**
     * @var int|null
     */
    public $executionTime = null;
}
