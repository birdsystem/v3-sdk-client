<?php

namespace BirdSystem\SDK\Client\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * Notice.
 */
class Notice extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var string|null
     */
    public $titleCn = null;

    /**
     * @var string|null
     */
    public $titleEn = null;

    /**
     * @var string|null
     */
    public $contentCn = null;

    /**
     * @var string|null
     */
    public $contentEn = null;

    /**
     * @var bool
     */
    public $isNeedConfirm = null;

    /**
     * @var string
     */
    public $createTime = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     */
    public $updateTime = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     */
    public $status = null;

    /**
     * @var bool
     */
    public $showAtHomePage = null;
}
