<?php

namespace BirdSystem\SDK\Client\Model\Client;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * Client.
 */
class AdminReadClientRead extends AbstractModel
{
    /**
     * @var \BirdSystem\SDK\Client\Model\ClientCompany\AdminReadClientRead[]
     */
    public $clientCompanies = null;

    /**
     * @var string[]
     */
    public $clientCompany = null;

    /**
     * @var int
     */
    public $id = null;

    /**
     * @var int|null
     */
    public $parentClientId = null;

    /**
     * @var string
     */
    public $name = null;

    /**
     * @var string|null
     */
    public $contact = null;

    /**
     * @var string|null
     */
    public $email = null;

    /**
     * @var string|null
     */
    public $financeEmail = null;

    /**
     * @var string|null
     */
    public $qq = null;

    /**
     * @var string|null
     */
    public $telephone = null;

    /**
     * @var int|null
     */
    public $customerServiceCompanyUserId = null;

    /**
     * @var int|null
     */
    public $normalServiceCompanyUserId = null;

    /**
     * @var string|null
     */
    public $addressLine1 = null;

    /**
     * @var string|null
     */
    public $addressLine2 = null;

    /**
     * @var string|null
     */
    public $addressLine3 = null;

    /**
     * @var string|null
     */
    public $city = null;

    /**
     * @var string|null
     */
    public $county = null;

    /**
     * @var string|null
     */
    public $postCode = null;

    /**
     * @var string|null
     */
    public $countryIso = null;

    /**
     * @var string
     */
    public $createTime = 'CURRENT_TIMESTAMP';

    /**
     * @var string|null
     */
    public $lastLoginTime = null;

    /**
     * @var string|null
     */
    public $nextChargingDate = null;

    /**
     * @var string|null
     */
    public $clientSupportExpireTime = null;

    /**
     * @var string|null
     */
    public $note = null;

    /**
     * @var string
     */
    public $bulkOrderReferenceField = 'CLIENT_REF';

    /**
     * @var bool
     */
    public $isReceiveNotificationSms = true;

    /**
     * @var bool
     */
    public $isReceiveNotificationEmail = true;

    /**
     * @var bool
     */
    public $isReceiveNotificationQqGroup = true;

    /**
     * @var bool
     */
    public $isSpontaneous = null;

    /**
     * @var bool
     */
    public $isSkipProductCheck = null;

    /**
     * @var bool
     */
    public $isEcppEnabled = null;

    /**
     * @var bool
     */
    public $dynamicWarehouseAutoTransferProduct = true;

    /**
     * @var int
     */
    public $seaContainerSkuMaximum = '10';

    /**
     * @var int
     */
    public $airContainerSkuMaximum = '15';

    /**
     * @var int
     */
    public $expressContainerSkuMaximum = '15';

    /**
     * @var int
     */
    public $expressLineContainerSkuMaximum = '15';

    /**
     * @var bool
     */
    public $clientDispatchChargeFromDesWarehouse = null;

    /**
     * @var string
     */
    public $clientType = 'UNKNOWN';

    /**
     * @var string
     */
    public $status = 'PENDING';
}
