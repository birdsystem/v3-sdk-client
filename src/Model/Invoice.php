<?php

namespace BirdSystem\SDK\Client\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * Invoice.
 */
class Invoice extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var int|null
     */
    public $serviceProviderId = null;

    /**
     * @var int|null
     */
    public $clientId = null;

    /**
     * @var int
     */
    public $companyUserId = null;

    /**
     * @var string
     */
    public $reference = null;

    /**
     * @var string|null
     */
    public $originReference = null;

    /**
     * @var string
     */
    public $amount = '0.000';

    /**
     * @var string
     */
    public $amountEstimate = '0.000';

    /**
     * @var string
     */
    public $amountCheck = '0.000';

    /**
     * @var string
     */
    public $grossProfit = '0.000';

    /**
     * @var int|null
     */
    public $grossProfitRate = null;

    /**
     * @var bool
     */
    public $isAlert = null;

    /**
     * @var string
     */
    public $checkStatus = null;

    /**
     * @var string|null
     */
    public $checkNote = null;

    /**
     * @var string
     */
    public $inOut = null;

    /**
     * @var string
     */
    public $billingStartTime = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     */
    public $billingEndTime = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     */
    public $createTime = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     */
    public $updateTime = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     */
    public $status = null;
}
