<?php

namespace BirdSystem\SDK\Client\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * InvoiceRelatedRecord.
 */
class InvoiceRelatedRecord extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var int
     */
    public $invoiceId = null;

    /**
     * @var string
     */
    public $recordId = null;

    /**
     * @var string
     */
    public $recordType = null;
}
