<?php

namespace BirdSystem\SDK\Client\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * ClientStatement.
 */
class ClientStatement extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var int
     */
    public $companyId = null;

    /**
     * @var int
     */
    public $clientId = null;

    /**
     * @var int|null
     */
    public $actualClientId = null;

    /**
     * @var string|null
     */
    public $recordId = null;

    /**
     * @var int|null
     */
    public $companyUserId = null;

    /**
     * @var string
     */
    public $amount = '0.00';

    /**
     * @var string
     */
    public $balance = '0.00';

    /**
     * @var string|null
     */
    public $note = null;

    /**
     * @var string
     */
    public $updateTime = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     */
    public $status = null;
}
