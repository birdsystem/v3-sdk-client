<?php

namespace BirdSystem\SDK\Client\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * ClientGroupSpontaneousDispatchFeeSetting.
 */
class ClientGroupSpontaneousDispatchFeeSetting extends AbstractModel
{
    /**
     * @var int
     */
    public $clientGroupId = null;

    /**
     * @var string
     */
    public $weight = null;

    /**
     * @var string
     */
    public $feePerItem = '0.00';

    /**
     * @var string
     */
    public $feePerSku = '0.00';
}
