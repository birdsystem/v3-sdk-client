<?php

namespace BirdSystem\SDK\Client\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * ClientCountryGuaranteeStatement.
 */
class ClientCountryGuaranteeStatement extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var int
     */
    public $clientId = null;

    /**
     * @var string
     */
    public $countryIso = null;

    /**
     * @var string
     */
    public $balance = '0.00';

    /**
     * @var string
     */
    public $amount = '0.00';

    /**
     * @var string
     */
    public $createTime = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     */
    public $note = null;
}
