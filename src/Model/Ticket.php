<?php

namespace BirdSystem\SDK\Client\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * Ticket.
 */
class Ticket extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var int
     */
    public $parentId = null;

    /**
     * @var int
     */
    public $companyId = null;

    /**
     * @var int|null
     */
    public $companyUserId = null;

    /**
     * @var int
     */
    public $clientId = null;

    /**
     * @var int|null
     */
    public $clientUserId = null;

    /**
     * @var string|null
     */
    public $sysxtype = null;

    /**
     * @var string|null
     */
    public $recordId = null;

    /**
     * @var string|null
     */
    public $subject = null;

    /**
     * @var string|null
     */
    public $content = null;

    /**
     * @var string
     */
    public $createTime = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     */
    public $status = null;
}
