<?php

namespace BirdSystem\SDK\Client\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * NoticeClient.
 */
class NoticeClient extends AbstractModel
{
    /**
     * @var int
     */
    public $noticeId = null;

    /**
     * @var int
     */
    public $clientId = null;

    /**
     * @var string|null
     */
    public $readTime = null;

    /**
     * @var string|null
     */
    public $confirmTime = null;

    /**
     * @var string|null
     */
    public $confirmStatus = null;
}
