<?php

namespace BirdSystem\SDK\Client\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * InvoiceItem.
 */
class InvoiceItem extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var int
     */
    public $invoiceId = null;

    /**
     * @var int
     */
    public $feeItemId = null;

    /**
     * @var string
     */
    public $quantity = '0.000';

    /**
     * @var string
     */
    public $quantityEstimate = '0.000';

    /**
     * @var string
     */
    public $quantityCheck = '0.000';

    /**
     * @var string
     */
    public $amount = '0.000';

    /**
     * @var string
     */
    public $amountEstimate = '0.000';

    /**
     * @var string
     */
    public $amountCheck = '0.000';

    /**
     * @var string|null
     */
    public $note = null;

    /**
     * @var string
     */
    public $createTime = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     */
    public $updateTime = 'CURRENT_TIMESTAMP';
}
