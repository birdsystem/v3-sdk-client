<?php

namespace BirdSystem\SDK\Client\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * NoticeClientConfirmDetail.
 */
class NoticeClientConfirmDetail extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var int
     */
    public $noticeId = null;

    /**
     * @var int|null
     */
    public $clientId = null;

    /**
     * @var int|null
     */
    public $companyUserId = null;

    /**
     * @var int|null
     */
    public $clientUserId = null;

    /**
     * @var string
     */
    public $content = null;

    /**
     * @var string
     */
    public $createTime = 'CURRENT_TIMESTAMP';
}
