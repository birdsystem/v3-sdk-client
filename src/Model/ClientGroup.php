<?php

namespace BirdSystem\SDK\Client\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * ClientGroup.
 */
class ClientGroup extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var int
     */
    public $companyId = null;

    /**
     * @var string|null
     */
    public $name = null;

    /**
     * @var int
     */
    public $clientDispatchGroupId = null;

    /**
     * @var string
     */
    public $handlingFeeLimit = '0.00';

    /**
     * @var string
     */
    public $handlingFeeFirstItem = '0.00';

    /**
     * @var string
     */
    public $handlingFeeOtherItem = '0.00';

    /**
     * @var string
     */
    public $handlingFeeFirstProduct = '0.00';

    /**
     * @var string
     */
    public $handlingFeeOtherProduct = '0.00';

    /**
     * @var string
     */
    public $deliveryFee = '0.00';

    /**
     * @var bool
     */
    public $deliveryFeeType = null;

    /**
     * @var string|null
     */
    public $clientDispatchVatChargeMethod = null;

    /**
     * @var string
     */
    public $clientDispatchVatChargeValue = '0.00';

    /**
     * @var string|null
     */
    public $clientDispatchImportDutyChargeMethod = null;

    /**
     * @var string
     */
    public $clientDispatchImportDutyChargeValue = '0.00';

    /**
     * @var string|null
     */
    public $clientTempStorageFeeMethod = null;

    /**
     * @var string
     */
    public $clientTempStorageFeeValue = '0.00';

    /**
     * @var string
     */
    public $monthlyMembershipFee = '0.00';

    /**
     * @var bool
     */
    public $creditLimitTopUpPeriod = '30';

    /**
     * @var int
     */
    public $creditLimitTopUpAmount = null;

    /**
     * @var bool
     */
    public $creditLimitTopUpTimes = true;

    /**
     * @var bool
     */
    public $creditLimitTopUpPeriodPerTime = true;

    /**
     * @var bool
     */
    public $limitDispatchIfBalanceNotEnough = null;

    /**
     * @var string|null
     */
    public $maxConsumption = null;

    /**
     * @var string
     */
    public $clientStockFeeMethod = null;

    /**
     * @var string|null
     */
    public $groupType = null;

    /**
     * @var int
     */
    public $clientStockFeeTotalVolumeMinimumHoldingDay = null;

    /**
     * @var string
     */
    public $clientStockFeeTotalVolumePrice = '0.00';

    /**
     * @var string
     */
    public $status = null;

    /**
     * @var string|null
     */
    public $note = null;
}
