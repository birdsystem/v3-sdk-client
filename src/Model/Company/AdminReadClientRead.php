<?php

namespace BirdSystem\SDK\Client\Model\Company;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

class AdminReadClientRead extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var string
     */
    public $name = null;

    /**
     * @var string|null
     */
    public $siteName = null;

    /**
     * @var string|null
     */
    public $email = null;

    /**
     * @var string|null
     */
    public $telephone = null;

    /**
     * @var string|null
     */
    public $addressLine1 = null;

    /**
     * @var string|null
     */
    public $addressLine2 = null;

    /**
     * @var string|null
     */
    public $addressLine3 = null;

    /**
     * @var string|null
     */
    public $city = null;

    /**
     * @var string|null
     */
    public $county = null;

    /**
     * @var string|null
     */
    public $postCode = null;

    /**
     * @var string|null
     */
    public $countryIso = null;

    /**
     * @var string|null
     */
    public $returnName = null;

    /**
     * @var string|null
     */
    public $returnAddressLine1 = null;

    /**
     * @var string|null
     */
    public $returnAddressLine2 = null;

    /**
     * @var string|null
     */
    public $returnAddressLine3 = null;

    /**
     * @var string|null
     */
    public $returnCity = null;

    /**
     * @var string|null
     */
    public $returnCounty = null;

    /**
     * @var string|null
     */
    public $returnPostCode = null;

    /**
     * @var string
     */
    public $returnCountryIso = null;

    /**
     * @var string|null
     */
    public $ladingBillName = null;

    /**
     * @var string|null
     */
    public $ladingBillTelephone = null;

    /**
     * @var string|null
     */
    public $ladingBillEmail = null;

    /**
     * @var string|null
     */
    public $ladingBillAddressLine1 = null;

    /**
     * @var string|null
     */
    public $ladingBillAddressLine2 = null;

    /**
     * @var string|null
     */
    public $ladingBillAddressLine3 = null;

    /**
     * @var string|null
     */
    public $ladingBillCity = null;

    /**
     * @var string|null
     */
    public $ladingBillCounty = null;

    /**
     * @var string|null
     */
    public $ladingBillPostCode = null;

    /**
     * @var string|null
     */
    public $ladingBillCountryIso = null;

    /**
     * @var string
     */
    public $settleTime = '00:00:00';

    /**
     * @var string
     */
    public $status = 'ACTIVE';

    /**
     * @var bool|null
     */
    public $isDynamicWarehouse = null;
}
