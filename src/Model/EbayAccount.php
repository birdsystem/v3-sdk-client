<?php

namespace BirdSystem\SDK\Client\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * EbayAccount.
 */
class EbayAccount extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var int
     */
    public $clientId = null;

    /**
     * @var int
     */
    public $companyId = null;

    /**
     * @var int
     */
    public $ebaySiteId = null;

    /**
     * @var string|null
     */
    public $ebayUserId = null;

    /**
     * @var string|null
     */
    public $token = null;

    /**
     * @var string|null
     */
    public $tokenExpirationTime = null;

    /**
     * @var string
     */
    public $updateTime = 'CURRENT_TIMESTAMP';
}
