<?php

namespace BirdSystem\SDK\Client\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * ClientStatementLock.
 */
class ClientStatementLock extends AbstractModel
{
    /**
     * @var int
     */
    public $companyId = null;

    /**
     * @var int
     */
    public $clientId = null;

    /**
     * @var int
     */
    public $value = null;
}
