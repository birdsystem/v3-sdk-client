<?php

namespace BirdSystem\SDK\Client\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * ClientUserGroupPrivilege.
 */
class ClientUserGroupPrivilege extends AbstractModel
{
    /**
     * @var int
     */
    public $clientUserGroupId = null;

    /**
     * @var string
     */
    public $privilegeCode = null;

    /**
     * @var bool
     */
    public $isDenied = null;

    /**
     * @var bool
     */
    public $isAllowed = null;
}
