<?php

namespace BirdSystem\SDK\Client\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * AmazonAccount.
 */
class AmazonAccount extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var int|null
     */
    public $companyId = null;

    /**
     * @var int|null
     */
    public $clientId = null;

    /**
     * @var string|null
     */
    public $name = null;

    /**
     * @var string
     */
    public $sellerId = null;

    /**
     * @var string
     */
    public $accountId = null;

    /**
     * @var string
     */
    public $marketplaceId = null;

    /**
     * @var string|null
     */
    public $mwsAuthToken = null;

    /**
     * @var string|null
     */
    public $awsAccessKeyId = null;

    /**
     * @var string|null
     */
    public $awsSecretAccessKey = null;

    /**
     * @var string|null
     */
    public $note = null;

    /**
     * @var string
     */
    public $createTime = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     */
    public $updateTime = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     */
    public $status = null;
}
