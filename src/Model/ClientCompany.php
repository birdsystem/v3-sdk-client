<?php

namespace BirdSystem\SDK\Client\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * ClientCompany.
 */
class ClientCompany extends AbstractModel
{
    public $client = null;

    public $company = null;

    /**
     * @var int
     */
    public $clientId = null;

    /**
     * @var int
     */
    public $companyId = null;

    /**
     * @var int
     */
    public $clientGroupId = null;

    /**
     * @var int|null
     */
    public $kpiClientGroupId = null;

    /**
     * @var bool
     */
    public $isInternal = true;

    /**
     * @var bool
     */
    public $isExternal = null;

    /**
     * @var bool
     */
    public $isVip = null;

    /**
     * @var string
     */
    public $balance = '0.00';

    /**
     * @var string
     */
    public $creditLimit = '0.00';

    /**
     * @var string
     */
    public $baseCreditLimit = '0.00';

    /**
     * @var string|null
     */
    public $baseCreditLimitExpiryTime = null;

    /**
     * @var string|null
     */
    public $returnName = null;

    /**
     * @var string|null
     */
    public $returnAddressLine1 = null;

    /**
     * @var string|null
     */
    public $returnAddressLine2 = null;

    /**
     * @var string|null
     */
    public $returnAddressLine3 = null;

    /**
     * @var string|null
     */
    public $returnCity = null;

    /**
     * @var string|null
     */
    public $returnCounty = null;

    /**
     * @var string|null
     */
    public $returnPostCode = null;

    /**
     * @var string|null
     */
    public $returnCountryIso = null;

    /**
     * @var string|null
     */
    public $senderName = null;

    /**
     * @var string|null
     */
    public $senderAddressLine1 = null;

    /**
     * @var string|null
     */
    public $senderAddressLine2 = null;

    /**
     * @var string|null
     */
    public $senderAddressLine3 = null;

    /**
     * @var string|null
     */
    public $senderCity = null;

    /**
     * @var string|null
     */
    public $senderCounty = null;

    /**
     * @var string|null
     */
    public $senderPostCode = null;

    /**
     * @var string|null
     */
    public $senderCountryIso = null;

    /**
     * @var bool|null
     */
    public $isServiceChargeApplicable = null;

    /**
     * @var string|null
     */
    public $serviceChargeManualSetUntilTime = null;

    /**
     * @var string|null
     */
    public $lastServiceChargeTime = null;

    /**
     * @var string
     */
    public $averageClientScore = '0.00';

    /**
     * @var bool
     */
    public $isAutoCalculateClientGroup = null;

    /**
     * @var bool
     */
    public $isStoUser = null;

    /**
     * @var string
     */
    public $status = 'ACTIVE';

    /**
     * @var string|null
     */
    public $lastLoginTime = null;
}
