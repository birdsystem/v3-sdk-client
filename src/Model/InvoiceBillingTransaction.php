<?php

namespace BirdSystem\SDK\Client\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * InvoiceBillingTransaction.
 */
class InvoiceBillingTransaction extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var int|null
     */
    public $invoiceId = null;

    /**
     * @var int|null
     */
    public $billingAccountId = null;

    /**
     * @var string
     */
    public $invoiceBillingTransaction = null;

    /**
     * @var string
     */
    public $amount = null;

    /**
     * @var string
     */
    public $currencyCode = null;

    /**
     * @var string
     */
    public $exchangeRate = '1.000';

    /**
     * @var string|null
     */
    public $note = null;

    /**
     * @var string
     */
    public $createTime = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     */
    public $updateTime = 'CURRENT_TIMESTAMP';
}
