<?php

namespace BirdSystem\SDK\Client\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * DailyStockStatement.
 */
class DailyStockStatement extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var int
     */
    public $companyId = null;

    /**
     * @var int
     */
    public $clientId = null;

    /**
     * @var string
     */
    public $date = null;

    /**
     * @var int
     */
    public $inventoryCycleDays = null;

    /**
     * @var string
     */
    public $stockVolume = '0.00';

    /**
     * @var string
     */
    public $perVolumeFee = '0.00';

    /**
     * @var string
     */
    public $additionalFee = '0.00';
}
