<?php

namespace BirdSystem\SDK\Client\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * LicenceAgreementClientConfirm.
 */
class LicenceAgreementClientConfirm extends AbstractModel
{
    /**
     * @var int
     */
    public $licenceAgreementId = null;

    /**
     * @var int
     */
    public $clientId = null;

    /**
     * @var int
     */
    public $clientUserId = null;

    /**
     * @var string
     */
    public $updateTime = 'CURRENT_TIMESTAMP';
}
