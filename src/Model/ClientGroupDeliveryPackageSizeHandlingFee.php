<?php

namespace BirdSystem\SDK\Client\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * ClientGroupDeliveryPackageSizeHandlingFee.
 */
class ClientGroupDeliveryPackageSizeHandlingFee extends AbstractModel
{
    /**
     * @var int
     */
    public $clientGroupId = null;

    /**
     * @var int
     */
    public $deliveryPackageSizeId = null;

    /**
     * @var string
     */
    public $handlingFeeLimit = '0.00';

    /**
     * @var string
     */
    public $handlingFeeFirstItem = '0.00';

    /**
     * @var string
     */
    public $handlingFeeOtherItem = '0.00';

    /**
     * @var string
     */
    public $handlingFeeFirstProduct = '0.00';

    /**
     * @var string
     */
    public $handlingFeeOtherProduct = '0.00';

    /**
     * @var bool
     */
    public $isUsed = true;
}
