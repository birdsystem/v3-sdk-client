<?php

namespace BirdSystem\SDK\Client\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * ClientCreditLimitTopUpTransaction.
 */
class ClientCreditLimitTopUpTransaction extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var int
     */
    public $companyId = null;

    /**
     * @var int
     */
    public $clientId = null;

    /**
     * @var int
     */
    public $amount = null;

    /**
     * @var string
     */
    public $updateTime = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     */
    public $status = null;
}
