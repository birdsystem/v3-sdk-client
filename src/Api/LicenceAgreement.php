<?php

namespace BirdSystem\SDK\Client\Api;

use BirdSystem\SDK\Client\Model\LicenceAgreement as LicenceAgreementModel;
use OpenAPI\Runtime\AbstractAPI as AbstractAPI;

class LicenceAgreement extends AbstractAPI
{
    /**
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *
     * @return LicenceAgreementModel[]
     */
    public function getCollection(array $queries = []): array
    {
        return $this->client->request('getLicenceAgreementCollection', 'get', '/api/client/licence_agreements',
            [
                'query' => $queries,
            ]
        );
    }

    /**
     * @param LicenceAgreementModel $Model The new LicenceAgreement resource
     *
     * @return LicenceAgreementModel
     */
    public function postCollection(LicenceAgreementModel $Model): LicenceAgreementModel
    {
        return $this->client->request('postLicenceAgreementCollection', 'post', '/api/client/licence_agreements',
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return LicenceAgreementModel
     */
    public function getItem(string $id): LicenceAgreementModel
    {
        return $this->client->request('getLicenceAgreementItem', 'get', "/api/client/licence_agreements/{$id}",
            [
            ]
        );
    }

    /**
     * @param string                $id
     * @param LicenceAgreementModel $Model The updated LicenceAgreement resource
     *
     * @return LicenceAgreementModel
     */
    public function putItem(string $id, LicenceAgreementModel $Model): LicenceAgreementModel
    {
        return $this->client->request('putLicenceAgreementItem', 'put', "/api/client/licence_agreements/{$id}",
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return mixed
     */
    public function deleteItem(string $id)
    {
        return $this->client->request('deleteLicenceAgreementItem', 'delete', "/api/client/licence_agreements/{$id}",
            [
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return LicenceAgreementModel
     */
    public function patchItem(string $id): LicenceAgreementModel
    {
        return $this->client->request('patchLicenceAgreementItem', 'patch', "/api/client/licence_agreements/{$id}",
            [
            ]
        );
    }
}
