<?php

namespace BirdSystem\SDK\Client\Api;

use BirdSystem\SDK\Client\Model\EbayAccount as EbayAccountModel;
use OpenAPI\Runtime\AbstractAPI as AbstractAPI;

class EbayAccount extends AbstractAPI
{
    /**
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *
     * @return EbayAccountModel[]
     */
    public function getCollection(array $queries = []): array
    {
        return $this->client->request('getEbayAccountCollection', 'get', '/api/client/ebay_accounts',
            [
                'query' => $queries,
            ]
        );
    }

    /**
     * @param EbayAccountModel $Model The new EbayAccount resource
     *
     * @return EbayAccountModel
     */
    public function postCollection(EbayAccountModel $Model): EbayAccountModel
    {
        return $this->client->request('postEbayAccountCollection', 'post', '/api/client/ebay_accounts',
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return EbayAccountModel
     */
    public function getItem(string $id): EbayAccountModel
    {
        return $this->client->request('getEbayAccountItem', 'get', "/api/client/ebay_accounts/{$id}",
            [
            ]
        );
    }

    /**
     * @param string           $id
     * @param EbayAccountModel $Model The updated EbayAccount resource
     *
     * @return EbayAccountModel
     */
    public function putItem(string $id, EbayAccountModel $Model): EbayAccountModel
    {
        return $this->client->request('putEbayAccountItem', 'put', "/api/client/ebay_accounts/{$id}",
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return mixed
     */
    public function deleteItem(string $id)
    {
        return $this->client->request('deleteEbayAccountItem', 'delete', "/api/client/ebay_accounts/{$id}",
            [
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return EbayAccountModel
     */
    public function patchItem(string $id): EbayAccountModel
    {
        return $this->client->request('patchEbayAccountItem', 'patch', "/api/client/ebay_accounts/{$id}",
            [
            ]
        );
    }
}
