<?php

namespace BirdSystem\SDK\Client\Api;

use BirdSystem\SDK\Client\Model\Ticket as TicketModel;
use OpenAPI\Runtime\AbstractAPI as AbstractAPI;

class Ticket extends AbstractAPI
{
    /**
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *
     * @return TicketModel[]
     */
    public function getCollection(array $queries = []): array
    {
        return $this->client->request('getTicketCollection', 'get', '/api/client/tickets',
            [
                'query' => $queries,
            ]
        );
    }

    /**
     * @param TicketModel $Model The new Ticket resource
     *
     * @return TicketModel
     */
    public function postCollection(TicketModel $Model): TicketModel
    {
        return $this->client->request('postTicketCollection', 'post', '/api/client/tickets',
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return TicketModel
     */
    public function getItem(string $id): TicketModel
    {
        return $this->client->request('getTicketItem', 'get', "/api/client/tickets/{$id}",
            [
            ]
        );
    }

    /**
     * @param string      $id
     * @param TicketModel $Model The updated Ticket resource
     *
     * @return TicketModel
     */
    public function putItem(string $id, TicketModel $Model): TicketModel
    {
        return $this->client->request('putTicketItem', 'put', "/api/client/tickets/{$id}",
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return mixed
     */
    public function deleteItem(string $id)
    {
        return $this->client->request('deleteTicketItem', 'delete', "/api/client/tickets/{$id}",
            [
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return TicketModel
     */
    public function patchItem(string $id): TicketModel
    {
        return $this->client->request('patchTicketItem', 'patch', "/api/client/tickets/{$id}",
            [
            ]
        );
    }
}
