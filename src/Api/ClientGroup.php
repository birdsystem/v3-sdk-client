<?php

namespace BirdSystem\SDK\Client\Api;

use BirdSystem\SDK\Client\Model\ClientGroup as ClientGroupModel;
use OpenAPI\Runtime\AbstractAPI as AbstractAPI;

class ClientGroup extends AbstractAPI
{
    /**
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *
     * @return ClientGroupModel[]
     */
    public function getCollection(array $queries = []): array
    {
        return $this->client->request('getClientGroupCollection', 'get', '/api/client/client_groups',
            [
                'query' => $queries,
            ]
        );
    }

    /**
     * @param ClientGroupModel $Model The new ClientGroup resource
     *
     * @return ClientGroupModel
     */
    public function postCollection(ClientGroupModel $Model): ClientGroupModel
    {
        return $this->client->request('postClientGroupCollection', 'post', '/api/client/client_groups',
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return ClientGroupModel
     */
    public function getItem(string $id): ClientGroupModel
    {
        return $this->client->request('getClientGroupItem', 'get', "/api/client/client_groups/{$id}",
            [
            ]
        );
    }

    /**
     * @param string           $id
     * @param ClientGroupModel $Model The updated ClientGroup resource
     *
     * @return ClientGroupModel
     */
    public function putItem(string $id, ClientGroupModel $Model): ClientGroupModel
    {
        return $this->client->request('putClientGroupItem', 'put', "/api/client/client_groups/{$id}",
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return mixed
     */
    public function deleteItem(string $id)
    {
        return $this->client->request('deleteClientGroupItem', 'delete', "/api/client/client_groups/{$id}",
            [
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return ClientGroupModel
     */
    public function patchItem(string $id): ClientGroupModel
    {
        return $this->client->request('patchClientGroupItem', 'patch', "/api/client/client_groups/{$id}",
            [
            ]
        );
    }
}
