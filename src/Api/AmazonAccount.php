<?php

namespace BirdSystem\SDK\Client\Api;

use BirdSystem\SDK\Client\Model\AmazonAccount as AmazonAccountModel;
use OpenAPI\Runtime\AbstractAPI as AbstractAPI;

class AmazonAccount extends AbstractAPI
{
    /**
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *
     * @return AmazonAccountModel[]
     */
    public function getCollection(array $queries = []): array
    {
        return $this->client->request('getAmazonAccountCollection', 'get', '/api/client/amazon_accounts',
            [
                'query' => $queries,
            ]
        );
    }

    /**
     * @param AmazonAccountModel $Model The new AmazonAccount resource
     *
     * @return AmazonAccountModel
     */
    public function postCollection(AmazonAccountModel $Model): AmazonAccountModel
    {
        return $this->client->request('postAmazonAccountCollection', 'post', '/api/client/amazon_accounts',
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return AmazonAccountModel
     */
    public function getItem(string $id): AmazonAccountModel
    {
        return $this->client->request('getAmazonAccountItem', 'get', "/api/client/amazon_accounts/{$id}",
            [
            ]
        );
    }

    /**
     * @param string             $id
     * @param AmazonAccountModel $Model The updated AmazonAccount resource
     *
     * @return AmazonAccountModel
     */
    public function putItem(string $id, AmazonAccountModel $Model): AmazonAccountModel
    {
        return $this->client->request('putAmazonAccountItem', 'put', "/api/client/amazon_accounts/{$id}",
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return mixed
     */
    public function deleteItem(string $id)
    {
        return $this->client->request('deleteAmazonAccountItem', 'delete', "/api/client/amazon_accounts/{$id}",
            [
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return AmazonAccountModel
     */
    public function patchItem(string $id): AmazonAccountModel
    {
        return $this->client->request('patchAmazonAccountItem', 'patch', "/api/client/amazon_accounts/{$id}",
            [
            ]
        );
    }
}
