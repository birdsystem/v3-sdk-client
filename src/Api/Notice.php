<?php

namespace BirdSystem\SDK\Client\Api;

use BirdSystem\SDK\Client\Model\Notice as NoticeModel;
use OpenAPI\Runtime\AbstractAPI as AbstractAPI;

class Notice extends AbstractAPI
{
    /**
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *
     * @return NoticeModel[]
     */
    public function getCollection(array $queries = []): array
    {
        return $this->client->request('getNoticeCollection', 'get', '/api/client/notices',
            [
                'query' => $queries,
            ]
        );
    }

    /**
     * @param NoticeModel $Model The new Notice resource
     *
     * @return NoticeModel
     */
    public function postCollection(NoticeModel $Model): NoticeModel
    {
        return $this->client->request('postNoticeCollection', 'post', '/api/client/notices',
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return NoticeModel
     */
    public function getItem(string $id): NoticeModel
    {
        return $this->client->request('getNoticeItem', 'get', "/api/client/notices/{$id}",
            [
            ]
        );
    }

    /**
     * @param string      $id
     * @param NoticeModel $Model The updated Notice resource
     *
     * @return NoticeModel
     */
    public function putItem(string $id, NoticeModel $Model): NoticeModel
    {
        return $this->client->request('putNoticeItem', 'put', "/api/client/notices/{$id}",
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return mixed
     */
    public function deleteItem(string $id)
    {
        return $this->client->request('deleteNoticeItem', 'delete', "/api/client/notices/{$id}",
            [
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return NoticeModel
     */
    public function patchItem(string $id): NoticeModel
    {
        return $this->client->request('patchNoticeItem', 'patch', "/api/client/notices/{$id}",
            [
            ]
        );
    }
}
