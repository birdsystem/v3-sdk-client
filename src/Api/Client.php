<?php

namespace BirdSystem\SDK\Client\Api;

use BirdSystem\SDK\Client\Model\Client\AdminReadClientRead as AdminReadClientRead;
use BirdSystem\SDK\Client\Model\Client as ClientModel;
use OpenAPI\Runtime\AbstractAPI as AbstractAPI;

class Client extends AbstractAPI
{
    /**
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *
     * @return AdminReadClientRead[]
     */
    public function getCollection(array $queries = []): array
    {
        return $this->client->request('getClientCollection', 'get', '/api/client/clients',
            [
                'query' => $queries,
            ]
        );
    }

    /**
     * @param ClientModel $Model The new Client resource
     *
     * @return AdminReadClientRead
     */
    public function postCollection(ClientModel $Model): AdminReadClientRead
    {
        return $this->client->request('postClientCollection', 'post', '/api/client/clients',
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return AdminReadClientRead
     */
    public function getItem(string $id): AdminReadClientRead
    {
        return $this->client->request('getClientItem', 'get', "/api/client/clients/{$id}",
            [
            ]
        );
    }

    /**
     * @param string      $id
     * @param ClientModel $Model The updated Client resource
     *
     * @return AdminReadClientRead
     */
    public function putItem(string $id, ClientModel $Model): AdminReadClientRead
    {
        return $this->client->request('putClientItem', 'put', "/api/client/clients/{$id}",
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return mixed
     */
    public function deleteItem(string $id)
    {
        return $this->client->request('deleteClientItem', 'delete', "/api/client/clients/{$id}",
            [
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return AdminReadClientRead
     */
    public function patchItem(string $id): AdminReadClientRead
    {
        return $this->client->request('patchClientItem', 'patch', "/api/client/clients/{$id}",
            [
            ]
        );
    }
}
