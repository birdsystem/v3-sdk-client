<?php

namespace BirdSystem\SDK\Client\Api;

use BirdSystem\SDK\Client\Model\ClientCountryGuaranteeStatement as ClientCountryGuaranteeStatementModel;
use OpenAPI\Runtime\AbstractAPI as AbstractAPI;

class ClientCountryGuaranteeStatement extends AbstractAPI
{
    /**
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *
     * @return ClientCountryGuaranteeStatementModel[]
     */
    public function getCollection(array $queries = []): array
    {
        return $this->client->request('getClientCountryGuaranteeStatementCollection', 'get', '/api/client/client_country_guarantee_statements',
            [
                'query' => $queries,
            ]
        );
    }

    /**
     * @param ClientCountryGuaranteeStatementModel $Model The new
     *                                                    ClientCountryGuaranteeStatement resource
     *
     * @return ClientCountryGuaranteeStatementModel
     */
    public function postCollection(ClientCountryGuaranteeStatementModel $Model): ClientCountryGuaranteeStatementModel
    {
        return $this->client->request('postClientCountryGuaranteeStatementCollection', 'post', '/api/client/client_country_guarantee_statements',
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return ClientCountryGuaranteeStatementModel
     */
    public function getItem(string $id): ClientCountryGuaranteeStatementModel
    {
        return $this->client->request('getClientCountryGuaranteeStatementItem', 'get', "/api/client/client_country_guarantee_statements/{$id}",
            [
            ]
        );
    }

    /**
     * @param string                               $id
     * @param ClientCountryGuaranteeStatementModel $Model The updated
     *                                                    ClientCountryGuaranteeStatement resource
     *
     * @return ClientCountryGuaranteeStatementModel
     */
    public function putItem(string $id, ClientCountryGuaranteeStatementModel $Model): ClientCountryGuaranteeStatementModel
    {
        return $this->client->request('putClientCountryGuaranteeStatementItem', 'put', "/api/client/client_country_guarantee_statements/{$id}",
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return mixed
     */
    public function deleteItem(string $id)
    {
        return $this->client->request('deleteClientCountryGuaranteeStatementItem', 'delete', "/api/client/client_country_guarantee_statements/{$id}",
            [
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return ClientCountryGuaranteeStatementModel
     */
    public function patchItem(string $id): ClientCountryGuaranteeStatementModel
    {
        return $this->client->request('patchClientCountryGuaranteeStatementItem', 'patch', "/api/client/client_country_guarantee_statements/{$id}",
            [
            ]
        );
    }
}
