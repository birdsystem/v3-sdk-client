<?php

namespace BirdSystem\SDK\Client\Api;

use BirdSystem\SDK\Client\Model\DailyStockStatement as DailyStockStatementModel;
use OpenAPI\Runtime\AbstractAPI as AbstractAPI;

class DailyStockStatement extends AbstractAPI
{
    /**
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *
     * @return DailyStockStatementModel[]
     */
    public function getCollection(array $queries = []): array
    {
        return $this->client->request('getDailyStockStatementCollection', 'get', '/api/client/daily_stock_statements',
            [
                'query' => $queries,
            ]
        );
    }

    /**
     * @param DailyStockStatementModel $Model The new DailyStockStatement resource
     *
     * @return DailyStockStatementModel
     */
    public function postCollection(DailyStockStatementModel $Model): DailyStockStatementModel
    {
        return $this->client->request('postDailyStockStatementCollection', 'post', '/api/client/daily_stock_statements',
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return DailyStockStatementModel
     */
    public function getItem(string $id): DailyStockStatementModel
    {
        return $this->client->request('getDailyStockStatementItem', 'get', "/api/client/daily_stock_statements/{$id}",
            [
            ]
        );
    }

    /**
     * @param string                   $id
     * @param DailyStockStatementModel $Model The updated DailyStockStatement resource
     *
     * @return DailyStockStatementModel
     */
    public function putItem(string $id, DailyStockStatementModel $Model): DailyStockStatementModel
    {
        return $this->client->request('putDailyStockStatementItem', 'put', "/api/client/daily_stock_statements/{$id}",
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return mixed
     */
    public function deleteItem(string $id)
    {
        return $this->client->request('deleteDailyStockStatementItem', 'delete', "/api/client/daily_stock_statements/{$id}",
            [
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return DailyStockStatementModel
     */
    public function patchItem(string $id): DailyStockStatementModel
    {
        return $this->client->request('patchDailyStockStatementItem', 'patch', "/api/client/daily_stock_statements/{$id}",
            [
            ]
        );
    }
}
