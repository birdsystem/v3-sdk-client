<?php

namespace BirdSystem\SDK\Client\Api;

use BirdSystem\SDK\Client\Model\ClientCreditLimitTopUpTransaction as ClientCreditLimitTopUpTransactionModel;
use OpenAPI\Runtime\AbstractAPI as AbstractAPI;

class ClientCreditLimitTopUpTransaction extends AbstractAPI
{
    /**
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *
     * @return ClientCreditLimitTopUpTransactionModel[]
     */
    public function getCollection(array $queries = []): array
    {
        return $this->client->request('getClientCreditLimitTopUpTransactionCollection', 'get', '/api/client/client_credit_limit_top_up_transactions',
            [
                'query' => $queries,
            ]
        );
    }

    /**
     * @param ClientCreditLimitTopUpTransactionModel $Model The new
     *                                                      ClientCreditLimitTopUpTransaction resource
     *
     * @return ClientCreditLimitTopUpTransactionModel
     */
    public function postCollection(ClientCreditLimitTopUpTransactionModel $Model): ClientCreditLimitTopUpTransactionModel
    {
        return $this->client->request('postClientCreditLimitTopUpTransactionCollection', 'post', '/api/client/client_credit_limit_top_up_transactions',
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return ClientCreditLimitTopUpTransactionModel
     */
    public function getItem(string $id): ClientCreditLimitTopUpTransactionModel
    {
        return $this->client->request('getClientCreditLimitTopUpTransactionItem', 'get', "/api/client/client_credit_limit_top_up_transactions/{$id}",
            [
            ]
        );
    }

    /**
     * @param string                                 $id
     * @param ClientCreditLimitTopUpTransactionModel $Model The updated
     *                                                      ClientCreditLimitTopUpTransaction resource
     *
     * @return ClientCreditLimitTopUpTransactionModel
     */
    public function putItem(string $id, ClientCreditLimitTopUpTransactionModel $Model): ClientCreditLimitTopUpTransactionModel
    {
        return $this->client->request('putClientCreditLimitTopUpTransactionItem', 'put', "/api/client/client_credit_limit_top_up_transactions/{$id}",
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return mixed
     */
    public function deleteItem(string $id)
    {
        return $this->client->request('deleteClientCreditLimitTopUpTransactionItem', 'delete', "/api/client/client_credit_limit_top_up_transactions/{$id}",
            [
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return ClientCreditLimitTopUpTransactionModel
     */
    public function patchItem(string $id): ClientCreditLimitTopUpTransactionModel
    {
        return $this->client->request('patchClientCreditLimitTopUpTransactionItem', 'patch', "/api/client/client_credit_limit_top_up_transactions/{$id}",
            [
            ]
        );
    }
}
