<?php

namespace BirdSystem\SDK\Client\Api;

use BirdSystem\SDK\Client\Model\ClientConfig as ClientConfigModel;
use OpenAPI\Runtime\AbstractAPI as AbstractAPI;

class ClientConfig extends AbstractAPI
{
    /**
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *
     * @return ClientConfigModel[]
     */
    public function getCollection(array $queries = []): array
    {
        return $this->client->request('getClientConfigCollection', 'get', '/api/client/client_configs',
            [
                'query' => $queries,
            ]
        );
    }

    /**
     * @param ClientConfigModel $Model The new ClientConfig resource
     *
     * @return ClientConfigModel
     */
    public function postCollection(ClientConfigModel $Model): ClientConfigModel
    {
        return $this->client->request('postClientConfigCollection', 'post', '/api/client/client_configs',
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return ClientConfigModel
     */
    public function getItem(string $id): ClientConfigModel
    {
        return $this->client->request('getClientConfigItem', 'get', "/api/client/client_configs/{$id}",
            [
            ]
        );
    }

    /**
     * @param string            $id
     * @param ClientConfigModel $Model The updated ClientConfig resource
     *
     * @return ClientConfigModel
     */
    public function putItem(string $id, ClientConfigModel $Model): ClientConfigModel
    {
        return $this->client->request('putClientConfigItem', 'put', "/api/client/client_configs/{$id}",
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return mixed
     */
    public function deleteItem(string $id)
    {
        return $this->client->request('deleteClientConfigItem', 'delete', "/api/client/client_configs/{$id}",
            [
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return ClientConfigModel
     */
    public function patchItem(string $id): ClientConfigModel
    {
        return $this->client->request('patchClientConfigItem', 'patch', "/api/client/client_configs/{$id}",
            [
            ]
        );
    }
}
