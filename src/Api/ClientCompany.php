<?php

namespace BirdSystem\SDK\Client\Api;

use BirdSystem\SDK\Client\Model\ClientCompany\AdminReadClientRead as AdminReadClientRead;
use OpenAPI\Runtime\AbstractAPI as AbstractAPI;

class ClientCompany extends AbstractAPI
{
    /**
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *
     * @return AdminReadClientRead[]
     */
    public function getCollection(array $queries = []): array
    {
        return $this->client->request('getClientCompanyCollection', 'get', '/api/client/client_companies',
            [
                'query' => $queries,
            ]
        );
    }

    /**
     * @param AdminReadClientRead $Model The new ClientCompany resource
     *
     * @return AdminReadClientRead
     */
    public function postCollection(AdminReadClientRead $Model): AdminReadClientRead
    {
        return $this->client->request('postClientCompanyCollection', 'post', '/api/client/client_companies',
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return AdminReadClientRead
     */
    public function getItem(string $id): AdminReadClientRead
    {
        return $this->client->request('getClientCompanyItem', 'get', "/api/client/client_companies/{$id}",
            [
            ]
        );
    }

    /**
     * @param string              $id
     * @param AdminReadClientRead $Model The updated ClientCompany resource
     *
     * @return AdminReadClientRead
     */
    public function putItem(string $id, AdminReadClientRead $Model): AdminReadClientRead
    {
        return $this->client->request('putClientCompanyItem', 'put', "/api/client/client_companies/{$id}",
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return mixed
     */
    public function deleteItem(string $id)
    {
        return $this->client->request('deleteClientCompanyItem', 'delete', "/api/client/client_companies/{$id}",
            [
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return AdminReadClientRead
     */
    public function patchItem(string $id): AdminReadClientRead
    {
        return $this->client->request('patchClientCompanyItem', 'patch', "/api/client/client_companies/{$id}",
            [
            ]
        );
    }
}
