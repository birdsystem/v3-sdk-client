<?php

namespace BirdSystem\SDK\Client\Api;

use BirdSystem\SDK\Client\Model\LicenceAgreementClientConfirm as LicenceAgreementClientConfirmModel;
use OpenAPI\Runtime\AbstractAPI as AbstractAPI;

class LicenceAgreementClientConfirm extends AbstractAPI
{
    /**
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *
     * @return LicenceAgreementClientConfirmModel[]
     */
    public function getCollection(array $queries = []): array
    {
        return $this->client->request('getLicenceAgreementClientConfirmCollection', 'get', '/api/client/licence_agreement_client_confirms',
            [
                'query' => $queries,
            ]
        );
    }

    /**
     * @param LicenceAgreementClientConfirmModel $Model The new
     *                                                  LicenceAgreementClientConfirm resource
     *
     * @return LicenceAgreementClientConfirmModel
     */
    public function postCollection(LicenceAgreementClientConfirmModel $Model): LicenceAgreementClientConfirmModel
    {
        return $this->client->request('postLicenceAgreementClientConfirmCollection', 'post', '/api/client/licence_agreement_client_confirms',
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return LicenceAgreementClientConfirmModel
     */
    public function getItem(string $id): LicenceAgreementClientConfirmModel
    {
        return $this->client->request('getLicenceAgreementClientConfirmItem', 'get', "/api/client/licence_agreement_client_confirms/{$id}",
            [
            ]
        );
    }

    /**
     * @param string                             $id
     * @param LicenceAgreementClientConfirmModel $Model The updated
     *                                                  LicenceAgreementClientConfirm resource
     *
     * @return LicenceAgreementClientConfirmModel
     */
    public function putItem(string $id, LicenceAgreementClientConfirmModel $Model): LicenceAgreementClientConfirmModel
    {
        return $this->client->request('putLicenceAgreementClientConfirmItem', 'put', "/api/client/licence_agreement_client_confirms/{$id}",
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return mixed
     */
    public function deleteItem(string $id)
    {
        return $this->client->request('deleteLicenceAgreementClientConfirmItem', 'delete', "/api/client/licence_agreement_client_confirms/{$id}",
            [
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return LicenceAgreementClientConfirmModel
     */
    public function patchItem(string $id): LicenceAgreementClientConfirmModel
    {
        return $this->client->request('patchLicenceAgreementClientConfirmItem', 'patch', "/api/client/licence_agreement_client_confirms/{$id}",
            [
            ]
        );
    }
}
