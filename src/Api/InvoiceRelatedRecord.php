<?php

namespace BirdSystem\SDK\Client\Api;

use BirdSystem\SDK\Client\Model\InvoiceRelatedRecord as InvoiceRelatedRecordModel;
use OpenAPI\Runtime\AbstractAPI as AbstractAPI;

class InvoiceRelatedRecord extends AbstractAPI
{
    /**
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *
     * @return InvoiceRelatedRecordModel[]
     */
    public function getCollection(array $queries = []): array
    {
        return $this->client->request('getInvoiceRelatedRecordCollection', 'get', '/api/client/invoice_related_records',
            [
                'query' => $queries,
            ]
        );
    }

    /**
     * @param InvoiceRelatedRecordModel $Model The new InvoiceRelatedRecord resource
     *
     * @return InvoiceRelatedRecordModel
     */
    public function postCollection(InvoiceRelatedRecordModel $Model): InvoiceRelatedRecordModel
    {
        return $this->client->request('postInvoiceRelatedRecordCollection', 'post', '/api/client/invoice_related_records',
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return InvoiceRelatedRecordModel
     */
    public function getItem(string $id): InvoiceRelatedRecordModel
    {
        return $this->client->request('getInvoiceRelatedRecordItem', 'get', "/api/client/invoice_related_records/{$id}",
            [
            ]
        );
    }

    /**
     * @param string                    $id
     * @param InvoiceRelatedRecordModel $Model The updated InvoiceRelatedRecord
     *                                         resource
     *
     * @return InvoiceRelatedRecordModel
     */
    public function putItem(string $id, InvoiceRelatedRecordModel $Model): InvoiceRelatedRecordModel
    {
        return $this->client->request('putInvoiceRelatedRecordItem', 'put', "/api/client/invoice_related_records/{$id}",
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return mixed
     */
    public function deleteItem(string $id)
    {
        return $this->client->request('deleteInvoiceRelatedRecordItem', 'delete', "/api/client/invoice_related_records/{$id}",
            [
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return InvoiceRelatedRecordModel
     */
    public function patchItem(string $id): InvoiceRelatedRecordModel
    {
        return $this->client->request('patchInvoiceRelatedRecordItem', 'patch', "/api/client/invoice_related_records/{$id}",
            [
            ]
        );
    }
}
