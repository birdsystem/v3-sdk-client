<?php

namespace BirdSystem\SDK\Client\Api;

use BirdSystem\SDK\Client\Model\ClientStatementLock as ClientStatementLockModel;
use OpenAPI\Runtime\AbstractAPI as AbstractAPI;

class ClientStatementLock extends AbstractAPI
{
    /**
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *
     * @return ClientStatementLockModel[]
     */
    public function getCollection(array $queries = []): array
    {
        return $this->client->request('getClientStatementLockCollection', 'get', '/api/client/client_statement_locks',
            [
                'query' => $queries,
            ]
        );
    }

    /**
     * @param ClientStatementLockModel $Model The new ClientStatementLock resource
     *
     * @return ClientStatementLockModel
     */
    public function postCollection(ClientStatementLockModel $Model): ClientStatementLockModel
    {
        return $this->client->request('postClientStatementLockCollection', 'post', '/api/client/client_statement_locks',
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return ClientStatementLockModel
     */
    public function getItem(string $id): ClientStatementLockModel
    {
        return $this->client->request('getClientStatementLockItem', 'get', "/api/client/client_statement_locks/{$id}",
            [
            ]
        );
    }

    /**
     * @param string                   $id
     * @param ClientStatementLockModel $Model The updated ClientStatementLock resource
     *
     * @return ClientStatementLockModel
     */
    public function putItem(string $id, ClientStatementLockModel $Model): ClientStatementLockModel
    {
        return $this->client->request('putClientStatementLockItem', 'put', "/api/client/client_statement_locks/{$id}",
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return mixed
     */
    public function deleteItem(string $id)
    {
        return $this->client->request('deleteClientStatementLockItem', 'delete', "/api/client/client_statement_locks/{$id}",
            [
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return ClientStatementLockModel
     */
    public function patchItem(string $id): ClientStatementLockModel
    {
        return $this->client->request('patchClientStatementLockItem', 'patch', "/api/client/client_statement_locks/{$id}",
            [
            ]
        );
    }
}
