<?php

namespace BirdSystem\SDK\Client\Api;

use BirdSystem\SDK\Client\Model\ClientAdditional as ClientAdditionalModel;
use OpenAPI\Runtime\AbstractAPI as AbstractAPI;

class ClientAdditional extends AbstractAPI
{
    /**
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *
     * @return ClientAdditionalModel[]
     */
    public function getCollection(array $queries = []): array
    {
        return $this->client->request('getClientAdditionalCollection', 'get', '/api/client/client_additionals',
            [
                'query' => $queries,
            ]
        );
    }

    /**
     * @param ClientAdditionalModel $Model The new ClientAdditional resource
     *
     * @return ClientAdditionalModel
     */
    public function postCollection(ClientAdditionalModel $Model): ClientAdditionalModel
    {
        return $this->client->request('postClientAdditionalCollection', 'post', '/api/client/client_additionals',
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $clientId
     *
     * @return ClientAdditionalModel
     */
    public function getItem(string $clientId): ClientAdditionalModel
    {
        return $this->client->request('getClientAdditionalItem', 'get', "/api/client/client_additionals/{$clientId}",
            [
            ]
        );
    }

    /**
     * @param string                $clientId
     * @param ClientAdditionalModel $Model    The updated ClientAdditional resource
     *
     * @return ClientAdditionalModel
     */
    public function putItem(string $clientId, ClientAdditionalModel $Model): ClientAdditionalModel
    {
        return $this->client->request('putClientAdditionalItem', 'put', "/api/client/client_additionals/{$clientId}",
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $clientId
     *
     * @return mixed
     */
    public function deleteItem(string $clientId)
    {
        return $this->client->request('deleteClientAdditionalItem', 'delete', "/api/client/client_additionals/{$clientId}",
            [
            ]
        );
    }

    /**
     * @param string $clientId
     *
     * @return ClientAdditionalModel
     */
    public function patchItem(string $clientId): ClientAdditionalModel
    {
        return $this->client->request('patchClientAdditionalItem', 'patch', "/api/client/client_additionals/{$clientId}",
            [
            ]
        );
    }
}
