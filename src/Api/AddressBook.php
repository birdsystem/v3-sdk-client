<?php

namespace BirdSystem\SDK\Client\Api;

use BirdSystem\SDK\Client\Model\AddressBook as AddressBookModel;
use OpenAPI\Runtime\AbstractAPI as AbstractAPI;

class AddressBook extends AbstractAPI
{
    /**
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *
     * @return AddressBookModel[]
     */
    public function getCollection(array $queries = []): array
    {
        return $this->client->request('getAddressBookCollection', 'get', '/api/client/address_books',
            [
                'query' => $queries,
            ]
        );
    }

    /**
     * @param AddressBookModel $Model The new AddressBook resource
     *
     * @return AddressBookModel
     */
    public function postCollection(AddressBookModel $Model): AddressBookModel
    {
        return $this->client->request('postAddressBookCollection', 'post', '/api/client/address_books',
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return AddressBookModel
     */
    public function getItem(string $id): AddressBookModel
    {
        return $this->client->request('getAddressBookItem', 'get', "/api/client/address_books/{$id}",
            [
            ]
        );
    }

    /**
     * @param string           $id
     * @param AddressBookModel $Model The updated AddressBook resource
     *
     * @return AddressBookModel
     */
    public function putItem(string $id, AddressBookModel $Model): AddressBookModel
    {
        return $this->client->request('putAddressBookItem', 'put', "/api/client/address_books/{$id}",
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return mixed
     */
    public function deleteItem(string $id)
    {
        return $this->client->request('deleteAddressBookItem', 'delete', "/api/client/address_books/{$id}",
            [
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return AddressBookModel
     */
    public function patchItem(string $id): AddressBookModel
    {
        return $this->client->request('patchAddressBookItem', 'patch', "/api/client/address_books/{$id}",
            [
            ]
        );
    }
}
