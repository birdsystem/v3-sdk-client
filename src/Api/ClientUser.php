<?php

namespace BirdSystem\SDK\Client\Api;

use BirdSystem\SDK\Client\Model\ClientUser as ClientUserModel;
use OpenAPI\Runtime\AbstractAPI as AbstractAPI;

class ClientUser extends AbstractAPI
{
    /**
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *
     * @return ClientUserModel[]
     */
    public function getCollection(array $queries = []): array
    {
        return $this->client->request('getClientUserCollection', 'get', '/api/client/client_users',
            [
                'query' => $queries,
            ]
        );
    }

    /**
     * @param ClientUserModel $Model The new ClientUser resource
     *
     * @return ClientUserModel
     */
    public function postCollection(ClientUserModel $Model): ClientUserModel
    {
        return $this->client->request('postClientUserCollection', 'post', '/api/client/client_users',
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return ClientUserModel
     */
    public function getItem(string $id): ClientUserModel
    {
        return $this->client->request('getClientUserItem', 'get', "/api/client/client_users/{$id}",
            [
            ]
        );
    }

    /**
     * @param string          $id
     * @param ClientUserModel $Model The updated ClientUser resource
     *
     * @return ClientUserModel
     */
    public function putItem(string $id, ClientUserModel $Model): ClientUserModel
    {
        return $this->client->request('putClientUserItem', 'put', "/api/client/client_users/{$id}",
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return mixed
     */
    public function deleteItem(string $id)
    {
        return $this->client->request('deleteClientUserItem', 'delete', "/api/client/client_users/{$id}",
            [
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return ClientUserModel
     */
    public function patchItem(string $id): ClientUserModel
    {
        return $this->client->request('patchClientUserItem', 'patch', "/api/client/client_users/{$id}",
            [
            ]
        );
    }
}
