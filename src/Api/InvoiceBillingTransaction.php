<?php

namespace BirdSystem\SDK\Client\Api;

use BirdSystem\SDK\Client\Model\InvoiceBillingTransaction as InvoiceBillingTransactionModel;
use OpenAPI\Runtime\AbstractAPI as AbstractAPI;

class InvoiceBillingTransaction extends AbstractAPI
{
    /**
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *
     * @return InvoiceBillingTransactionModel[]
     */
    public function getCollection(array $queries = []): array
    {
        return $this->client->request('getInvoiceBillingTransactionCollection', 'get', '/api/client/invoice_billing_transactions',
            [
                'query' => $queries,
            ]
        );
    }

    /**
     * @param InvoiceBillingTransactionModel $Model The new InvoiceBillingTransaction
     *                                              resource
     *
     * @return InvoiceBillingTransactionModel
     */
    public function postCollection(InvoiceBillingTransactionModel $Model): InvoiceBillingTransactionModel
    {
        return $this->client->request('postInvoiceBillingTransactionCollection', 'post', '/api/client/invoice_billing_transactions',
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return InvoiceBillingTransactionModel
     */
    public function getItem(string $id): InvoiceBillingTransactionModel
    {
        return $this->client->request('getInvoiceBillingTransactionItem', 'get', "/api/client/invoice_billing_transactions/{$id}",
            [
            ]
        );
    }

    /**
     * @param string                         $id
     * @param InvoiceBillingTransactionModel $Model The updated
     *                                              InvoiceBillingTransaction resource
     *
     * @return InvoiceBillingTransactionModel
     */
    public function putItem(string $id, InvoiceBillingTransactionModel $Model): InvoiceBillingTransactionModel
    {
        return $this->client->request('putInvoiceBillingTransactionItem', 'put', "/api/client/invoice_billing_transactions/{$id}",
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return mixed
     */
    public function deleteItem(string $id)
    {
        return $this->client->request('deleteInvoiceBillingTransactionItem', 'delete', "/api/client/invoice_billing_transactions/{$id}",
            [
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return InvoiceBillingTransactionModel
     */
    public function patchItem(string $id): InvoiceBillingTransactionModel
    {
        return $this->client->request('patchInvoiceBillingTransactionItem', 'patch', "/api/client/invoice_billing_transactions/{$id}",
            [
            ]
        );
    }
}
