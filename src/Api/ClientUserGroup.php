<?php

namespace BirdSystem\SDK\Client\Api;

use BirdSystem\SDK\Client\Model\ClientUserGroup as ClientUserGroupModel;
use OpenAPI\Runtime\AbstractAPI as AbstractAPI;

class ClientUserGroup extends AbstractAPI
{
    /**
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *
     * @return ClientUserGroupModel[]
     */
    public function getCollection(array $queries = []): array
    {
        return $this->client->request('getClientUserGroupCollection', 'get', '/api/client/client_user_groups',
            [
                'query' => $queries,
            ]
        );
    }

    /**
     * @param ClientUserGroupModel $Model The new ClientUserGroup resource
     *
     * @return ClientUserGroupModel
     */
    public function postCollection(ClientUserGroupModel $Model): ClientUserGroupModel
    {
        return $this->client->request('postClientUserGroupCollection', 'post', '/api/client/client_user_groups',
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return ClientUserGroupModel
     */
    public function getItem(string $id): ClientUserGroupModel
    {
        return $this->client->request('getClientUserGroupItem', 'get', "/api/client/client_user_groups/{$id}",
            [
            ]
        );
    }

    /**
     * @param string               $id
     * @param ClientUserGroupModel $Model The updated ClientUserGroup resource
     *
     * @return ClientUserGroupModel
     */
    public function putItem(string $id, ClientUserGroupModel $Model): ClientUserGroupModel
    {
        return $this->client->request('putClientUserGroupItem', 'put', "/api/client/client_user_groups/{$id}",
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return mixed
     */
    public function deleteItem(string $id)
    {
        return $this->client->request('deleteClientUserGroupItem', 'delete', "/api/client/client_user_groups/{$id}",
            [
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return ClientUserGroupModel
     */
    public function patchItem(string $id): ClientUserGroupModel
    {
        return $this->client->request('patchClientUserGroupItem', 'patch', "/api/client/client_user_groups/{$id}",
            [
            ]
        );
    }
}
