<?php

namespace BirdSystem\SDK\Client\Api;

use BirdSystem\SDK\Client\Model\ClientGroupDeliveryServiceHandlingFee as ClientGroupDeliveryServiceHandlingFeeModel;
use OpenAPI\Runtime\AbstractAPI as AbstractAPI;

class ClientGroupDeliveryServiceHandlingFee extends AbstractAPI
{
    /**
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *
     * @return ClientGroupDeliveryServiceHandlingFeeModel[]
     */
    public function getCollection(array $queries = []): array
    {
        return $this->client->request('getClientGroupDeliveryServiceHandlingFeeCollection', 'get', '/api/client/client_group_delivery_service_handling_fees',
            [
                'query' => $queries,
            ]
        );
    }

    /**
     * @param ClientGroupDeliveryServiceHandlingFeeModel $Model The new
     *                                                          ClientGroupDeliveryServiceHandlingFee resource
     *
     * @return ClientGroupDeliveryServiceHandlingFeeModel
     */
    public function postCollection(ClientGroupDeliveryServiceHandlingFeeModel $Model): ClientGroupDeliveryServiceHandlingFeeModel
    {
        return $this->client->request('postClientGroupDeliveryServiceHandlingFeeCollection', 'post', '/api/client/client_group_delivery_service_handling_fees',
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return ClientGroupDeliveryServiceHandlingFeeModel
     */
    public function getItem(string $id): ClientGroupDeliveryServiceHandlingFeeModel
    {
        return $this->client->request('getClientGroupDeliveryServiceHandlingFeeItem', 'get', "/api/client/client_group_delivery_service_handling_fees/{$id}",
            [
            ]
        );
    }

    /**
     * @param string                                     $id
     * @param ClientGroupDeliveryServiceHandlingFeeModel $Model The updated
     *                                                          ClientGroupDeliveryServiceHandlingFee resource
     *
     * @return ClientGroupDeliveryServiceHandlingFeeModel
     */
    public function putItem(string $id, ClientGroupDeliveryServiceHandlingFeeModel $Model): ClientGroupDeliveryServiceHandlingFeeModel
    {
        return $this->client->request('putClientGroupDeliveryServiceHandlingFeeItem', 'put', "/api/client/client_group_delivery_service_handling_fees/{$id}",
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return mixed
     */
    public function deleteItem(string $id)
    {
        return $this->client->request('deleteClientGroupDeliveryServiceHandlingFeeItem', 'delete', "/api/client/client_group_delivery_service_handling_fees/{$id}",
            [
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return ClientGroupDeliveryServiceHandlingFeeModel
     */
    public function patchItem(string $id): ClientGroupDeliveryServiceHandlingFeeModel
    {
        return $this->client->request('patchClientGroupDeliveryServiceHandlingFeeItem', 'patch', "/api/client/client_group_delivery_service_handling_fees/{$id}",
            [
            ]
        );
    }
}
