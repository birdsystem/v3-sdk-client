<?php

namespace BirdSystem\SDK\Client\Api;

use BirdSystem\SDK\Client\Model\Invoice as InvoiceModel;
use OpenAPI\Runtime\AbstractAPI as AbstractAPI;

class Invoice extends AbstractAPI
{
    /**
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *
     * @return InvoiceModel[]
     */
    public function getCollection(array $queries = []): array
    {
        return $this->client->request('getInvoiceCollection', 'get', '/api/client/invoices',
            [
                'query' => $queries,
            ]
        );
    }

    /**
     * @param InvoiceModel $Model The new Invoice resource
     *
     * @return InvoiceModel
     */
    public function postCollection(InvoiceModel $Model): InvoiceModel
    {
        return $this->client->request('postInvoiceCollection', 'post', '/api/client/invoices',
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return InvoiceModel
     */
    public function getItem(string $id): InvoiceModel
    {
        return $this->client->request('getInvoiceItem', 'get', "/api/client/invoices/{$id}",
            [
            ]
        );
    }

    /**
     * @param string       $id
     * @param InvoiceModel $Model The updated Invoice resource
     *
     * @return InvoiceModel
     */
    public function putItem(string $id, InvoiceModel $Model): InvoiceModel
    {
        return $this->client->request('putInvoiceItem', 'put', "/api/client/invoices/{$id}",
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return mixed
     */
    public function deleteItem(string $id)
    {
        return $this->client->request('deleteInvoiceItem', 'delete', "/api/client/invoices/{$id}",
            [
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return InvoiceModel
     */
    public function patchItem(string $id): InvoiceModel
    {
        return $this->client->request('patchInvoiceItem', 'patch', "/api/client/invoices/{$id}",
            [
            ]
        );
    }
}
