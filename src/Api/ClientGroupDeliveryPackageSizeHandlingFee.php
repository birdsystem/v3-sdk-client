<?php

namespace BirdSystem\SDK\Client\Api;

use BirdSystem\SDK\Client\Model\ClientGroupDeliveryPackageSizeHandlingFee as ClientGroupDeliveryPackageSizeHandlingFeeModel;
use OpenAPI\Runtime\AbstractAPI as AbstractAPI;

class ClientGroupDeliveryPackageSizeHandlingFee extends AbstractAPI
{
    /**
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *
     * @return ClientGroupDeliveryPackageSizeHandlingFeeModel[]
     */
    public function getCollection(array $queries = []): array
    {
        return $this->client->request('getClientGroupDeliveryPackageSizeHandlingFeeCollection', 'get', '/api/client/client_group_delivery_package_size_handling_fees',
            [
                'query' => $queries,
            ]
        );
    }

    /**
     * @param ClientGroupDeliveryPackageSizeHandlingFeeModel $Model The new
     *                                                              ClientGroupDeliveryPackageSizeHandlingFee resource
     *
     * @return ClientGroupDeliveryPackageSizeHandlingFeeModel
     */
    public function postCollection(ClientGroupDeliveryPackageSizeHandlingFeeModel $Model): ClientGroupDeliveryPackageSizeHandlingFeeModel
    {
        return $this->client->request('postClientGroupDeliveryPackageSizeHandlingFeeCollection', 'post', '/api/client/client_group_delivery_package_size_handling_fees',
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return ClientGroupDeliveryPackageSizeHandlingFeeModel
     */
    public function getItem(string $id): ClientGroupDeliveryPackageSizeHandlingFeeModel
    {
        return $this->client->request('getClientGroupDeliveryPackageSizeHandlingFeeItem', 'get', "/api/client/client_group_delivery_package_size_handling_fees/{$id}",
            [
            ]
        );
    }

    /**
     * @param string                                         $id
     * @param ClientGroupDeliveryPackageSizeHandlingFeeModel $Model The updated
     *                                                              ClientGroupDeliveryPackageSizeHandlingFee resource
     *
     * @return ClientGroupDeliveryPackageSizeHandlingFeeModel
     */
    public function putItem(string $id, ClientGroupDeliveryPackageSizeHandlingFeeModel $Model): ClientGroupDeliveryPackageSizeHandlingFeeModel
    {
        return $this->client->request('putClientGroupDeliveryPackageSizeHandlingFeeItem', 'put', "/api/client/client_group_delivery_package_size_handling_fees/{$id}",
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return mixed
     */
    public function deleteItem(string $id)
    {
        return $this->client->request('deleteClientGroupDeliveryPackageSizeHandlingFeeItem', 'delete', "/api/client/client_group_delivery_package_size_handling_fees/{$id}",
            [
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return ClientGroupDeliveryPackageSizeHandlingFeeModel
     */
    public function patchItem(string $id): ClientGroupDeliveryPackageSizeHandlingFeeModel
    {
        return $this->client->request('patchClientGroupDeliveryPackageSizeHandlingFeeItem', 'patch', "/api/client/client_group_delivery_package_size_handling_fees/{$id}",
            [
            ]
        );
    }
}
