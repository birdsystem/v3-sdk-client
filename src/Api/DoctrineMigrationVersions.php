<?php

namespace BirdSystem\SDK\Client\Api;

use BirdSystem\SDK\Client\Model\DoctrineMigrationVersions as DoctrineMigrationVersionsModel;
use OpenAPI\Runtime\AbstractAPI as AbstractAPI;

class DoctrineMigrationVersions extends AbstractAPI
{
    /**
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *
     * @return DoctrineMigrationVersionsModel[]
     */
    public function getCollection(array $queries = []): array
    {
        return $this->client->request('getDoctrineMigrationVersionsCollection', 'get', '/api/client/doctrine_migration_versions',
            [
                'query' => $queries,
            ]
        );
    }

    /**
     * @param DoctrineMigrationVersionsModel $Model The new DoctrineMigrationVersions
     *                                              resource
     *
     * @return DoctrineMigrationVersionsModel
     */
    public function postCollection(DoctrineMigrationVersionsModel $Model): DoctrineMigrationVersionsModel
    {
        return $this->client->request('postDoctrineMigrationVersionsCollection', 'post', '/api/client/doctrine_migration_versions',
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $version
     *
     * @return DoctrineMigrationVersionsModel
     */
    public function getItem(string $version): DoctrineMigrationVersionsModel
    {
        return $this->client->request('getDoctrineMigrationVersionsItem', 'get', "/api/client/doctrine_migration_versions/{$version}",
            [
            ]
        );
    }

    /**
     * @param string                         $version
     * @param DoctrineMigrationVersionsModel $Model   The updated
     *                                                DoctrineMigrationVersions resource
     *
     * @return DoctrineMigrationVersionsModel
     */
    public function putItem(string $version, DoctrineMigrationVersionsModel $Model): DoctrineMigrationVersionsModel
    {
        return $this->client->request('putDoctrineMigrationVersionsItem', 'put', "/api/client/doctrine_migration_versions/{$version}",
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $version
     *
     * @return mixed
     */
    public function deleteItem(string $version)
    {
        return $this->client->request('deleteDoctrineMigrationVersionsItem', 'delete', "/api/client/doctrine_migration_versions/{$version}",
            [
            ]
        );
    }

    /**
     * @param string $version
     *
     * @return DoctrineMigrationVersionsModel
     */
    public function patchItem(string $version): DoctrineMigrationVersionsModel
    {
        return $this->client->request('patchDoctrineMigrationVersionsItem', 'patch', "/api/client/doctrine_migration_versions/{$version}",
            [
            ]
        );
    }
}
