<?php

namespace BirdSystem\SDK\Client\Api;

use BirdSystem\SDK\Client\Model\InvoiceReference as InvoiceReferenceModel;
use OpenAPI\Runtime\AbstractAPI as AbstractAPI;

class InvoiceReference extends AbstractAPI
{
    /**
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *
     * @return InvoiceReferenceModel[]
     */
    public function getCollection(array $queries = []): array
    {
        return $this->client->request('getInvoiceReferenceCollection', 'get', '/api/client/invoice_references',
            [
                'query' => $queries,
            ]
        );
    }

    /**
     * @param InvoiceReferenceModel $Model The new InvoiceReference resource
     *
     * @return InvoiceReferenceModel
     */
    public function postCollection(InvoiceReferenceModel $Model): InvoiceReferenceModel
    {
        return $this->client->request('postInvoiceReferenceCollection', 'post', '/api/client/invoice_references',
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return InvoiceReferenceModel
     */
    public function getItem(string $id): InvoiceReferenceModel
    {
        return $this->client->request('getInvoiceReferenceItem', 'get', "/api/client/invoice_references/{$id}",
            [
            ]
        );
    }

    /**
     * @param string                $id
     * @param InvoiceReferenceModel $Model The updated InvoiceReference resource
     *
     * @return InvoiceReferenceModel
     */
    public function putItem(string $id, InvoiceReferenceModel $Model): InvoiceReferenceModel
    {
        return $this->client->request('putInvoiceReferenceItem', 'put', "/api/client/invoice_references/{$id}",
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return mixed
     */
    public function deleteItem(string $id)
    {
        return $this->client->request('deleteInvoiceReferenceItem', 'delete', "/api/client/invoice_references/{$id}",
            [
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return InvoiceReferenceModel
     */
    public function patchItem(string $id): InvoiceReferenceModel
    {
        return $this->client->request('patchInvoiceReferenceItem', 'patch', "/api/client/invoice_references/{$id}",
            [
            ]
        );
    }
}
