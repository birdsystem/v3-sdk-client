<?php

namespace BirdSystem\SDK\Client\Api;

use OpenAPI\Runtime\AbstractAPI as AbstractAPI;

class Token extends AbstractAPI
{
    /**
     * Create new JWT Token.
     *
     * @return mixed
     */
    public function getJwt()
    {
        return $this->client->request('getJwtToken', 'post', '/api/client/token',
            [
            ]
        );
    }
}
