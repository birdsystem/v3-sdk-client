<?php

namespace BirdSystem\SDK\Client\Api;

use BirdSystem\SDK\Client\Model\ClientGroupSpontaneousDispatchFeeSetting as ClientGroupSpontaneousDispatchFeeSettingModel;
use OpenAPI\Runtime\AbstractAPI as AbstractAPI;

class ClientGroupSpontaneousDispatchFeeSetting extends AbstractAPI
{
    /**
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *
     * @return ClientGroupSpontaneousDispatchFeeSettingModel[]
     */
    public function getCollection(array $queries = []): array
    {
        return $this->client->request('getClientGroupSpontaneousDispatchFeeSettingCollection', 'get', '/api/client/client_group_spontaneous_dispatch_fee_settings',
            [
                'query' => $queries,
            ]
        );
    }

    /**
     * @param ClientGroupSpontaneousDispatchFeeSettingModel $Model The new
     *                                                             ClientGroupSpontaneousDispatchFeeSetting resource
     *
     * @return ClientGroupSpontaneousDispatchFeeSettingModel
     */
    public function postCollection(ClientGroupSpontaneousDispatchFeeSettingModel $Model): ClientGroupSpontaneousDispatchFeeSettingModel
    {
        return $this->client->request('postClientGroupSpontaneousDispatchFeeSettingCollection', 'post', '/api/client/client_group_spontaneous_dispatch_fee_settings',
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return ClientGroupSpontaneousDispatchFeeSettingModel
     */
    public function getItem(string $id): ClientGroupSpontaneousDispatchFeeSettingModel
    {
        return $this->client->request('getClientGroupSpontaneousDispatchFeeSettingItem', 'get', "/api/client/client_group_spontaneous_dispatch_fee_settings/{$id}",
            [
            ]
        );
    }

    /**
     * @param string                                        $id
     * @param ClientGroupSpontaneousDispatchFeeSettingModel $Model The updated
     *                                                             ClientGroupSpontaneousDispatchFeeSetting resource
     *
     * @return ClientGroupSpontaneousDispatchFeeSettingModel
     */
    public function putItem(string $id, ClientGroupSpontaneousDispatchFeeSettingModel $Model): ClientGroupSpontaneousDispatchFeeSettingModel
    {
        return $this->client->request('putClientGroupSpontaneousDispatchFeeSettingItem', 'put', "/api/client/client_group_spontaneous_dispatch_fee_settings/{$id}",
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return mixed
     */
    public function deleteItem(string $id)
    {
        return $this->client->request('deleteClientGroupSpontaneousDispatchFeeSettingItem', 'delete', "/api/client/client_group_spontaneous_dispatch_fee_settings/{$id}",
            [
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return ClientGroupSpontaneousDispatchFeeSettingModel
     */
    public function patchItem(string $id): ClientGroupSpontaneousDispatchFeeSettingModel
    {
        return $this->client->request('patchClientGroupSpontaneousDispatchFeeSettingItem', 'patch', "/api/client/client_group_spontaneous_dispatch_fee_settings/{$id}",
            [
            ]
        );
    }
}
