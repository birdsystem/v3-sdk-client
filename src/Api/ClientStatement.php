<?php

namespace BirdSystem\SDK\Client\Api;

use BirdSystem\SDK\Client\Model\ClientStatement as ClientStatementModel;
use OpenAPI\Runtime\AbstractAPI as AbstractAPI;

class ClientStatement extends AbstractAPI
{
    /**
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *
     * @return ClientStatementModel[]
     */
    public function getCollection(array $queries = []): array
    {
        return $this->client->request('getClientStatementCollection', 'get', '/api/client/client_statements',
            [
                'query' => $queries,
            ]
        );
    }

    /**
     * @param ClientStatementModel $Model The new ClientStatement resource
     *
     * @return ClientStatementModel
     */
    public function postCollection(ClientStatementModel $Model): ClientStatementModel
    {
        return $this->client->request('postClientStatementCollection', 'post', '/api/client/client_statements',
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return ClientStatementModel
     */
    public function getItem(string $id): ClientStatementModel
    {
        return $this->client->request('getClientStatementItem', 'get', "/api/client/client_statements/{$id}",
            [
            ]
        );
    }

    /**
     * @param string               $id
     * @param ClientStatementModel $Model The updated ClientStatement resource
     *
     * @return ClientStatementModel
     */
    public function putItem(string $id, ClientStatementModel $Model): ClientStatementModel
    {
        return $this->client->request('putClientStatementItem', 'put', "/api/client/client_statements/{$id}",
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return mixed
     */
    public function deleteItem(string $id)
    {
        return $this->client->request('deleteClientStatementItem', 'delete', "/api/client/client_statements/{$id}",
            [
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return ClientStatementModel
     */
    public function patchItem(string $id): ClientStatementModel
    {
        return $this->client->request('patchClientStatementItem', 'patch', "/api/client/client_statements/{$id}",
            [
            ]
        );
    }
}
