<?php

namespace BirdSystem\SDK\Client\Api;

use BirdSystem\SDK\Client\Model\NoticeClientConfirmDetail as NoticeClientConfirmDetailModel;
use OpenAPI\Runtime\AbstractAPI as AbstractAPI;

class NoticeClientConfirmDetail extends AbstractAPI
{
    /**
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *
     * @return NoticeClientConfirmDetailModel[]
     */
    public function getCollection(array $queries = []): array
    {
        return $this->client->request('getNoticeClientConfirmDetailCollection', 'get', '/api/client/notice_client_confirm_details',
            [
                'query' => $queries,
            ]
        );
    }

    /**
     * @param NoticeClientConfirmDetailModel $Model The new NoticeClientConfirmDetail
     *                                              resource
     *
     * @return NoticeClientConfirmDetailModel
     */
    public function postCollection(NoticeClientConfirmDetailModel $Model): NoticeClientConfirmDetailModel
    {
        return $this->client->request('postNoticeClientConfirmDetailCollection', 'post', '/api/client/notice_client_confirm_details',
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return NoticeClientConfirmDetailModel
     */
    public function getItem(string $id): NoticeClientConfirmDetailModel
    {
        return $this->client->request('getNoticeClientConfirmDetailItem', 'get', "/api/client/notice_client_confirm_details/{$id}",
            [
            ]
        );
    }

    /**
     * @param string                         $id
     * @param NoticeClientConfirmDetailModel $Model The updated
     *                                              NoticeClientConfirmDetail resource
     *
     * @return NoticeClientConfirmDetailModel
     */
    public function putItem(string $id, NoticeClientConfirmDetailModel $Model): NoticeClientConfirmDetailModel
    {
        return $this->client->request('putNoticeClientConfirmDetailItem', 'put', "/api/client/notice_client_confirm_details/{$id}",
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return mixed
     */
    public function deleteItem(string $id)
    {
        return $this->client->request('deleteNoticeClientConfirmDetailItem', 'delete', "/api/client/notice_client_confirm_details/{$id}",
            [
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return NoticeClientConfirmDetailModel
     */
    public function patchItem(string $id): NoticeClientConfirmDetailModel
    {
        return $this->client->request('patchNoticeClientConfirmDetailItem', 'patch', "/api/client/notice_client_confirm_details/{$id}",
            [
            ]
        );
    }
}
