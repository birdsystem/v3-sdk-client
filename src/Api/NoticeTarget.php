<?php

namespace BirdSystem\SDK\Client\Api;

use BirdSystem\SDK\Client\Model\NoticeTarget as NoticeTargetModel;
use OpenAPI\Runtime\AbstractAPI as AbstractAPI;

class NoticeTarget extends AbstractAPI
{
    /**
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *
     * @return NoticeTargetModel[]
     */
    public function getCollection(array $queries = []): array
    {
        return $this->client->request('getNoticeTargetCollection', 'get', '/api/client/notice_targets',
            [
                'query' => $queries,
            ]
        );
    }

    /**
     * @param NoticeTargetModel $Model The new NoticeTarget resource
     *
     * @return NoticeTargetModel
     */
    public function postCollection(NoticeTargetModel $Model): NoticeTargetModel
    {
        return $this->client->request('postNoticeTargetCollection', 'post', '/api/client/notice_targets',
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return NoticeTargetModel
     */
    public function getItem(string $id): NoticeTargetModel
    {
        return $this->client->request('getNoticeTargetItem', 'get', "/api/client/notice_targets/{$id}",
            [
            ]
        );
    }

    /**
     * @param string            $id
     * @param NoticeTargetModel $Model The updated NoticeTarget resource
     *
     * @return NoticeTargetModel
     */
    public function putItem(string $id, NoticeTargetModel $Model): NoticeTargetModel
    {
        return $this->client->request('putNoticeTargetItem', 'put', "/api/client/notice_targets/{$id}",
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return mixed
     */
    public function deleteItem(string $id)
    {
        return $this->client->request('deleteNoticeTargetItem', 'delete', "/api/client/notice_targets/{$id}",
            [
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return NoticeTargetModel
     */
    public function patchItem(string $id): NoticeTargetModel
    {
        return $this->client->request('patchNoticeTargetItem', 'patch', "/api/client/notice_targets/{$id}",
            [
            ]
        );
    }
}
