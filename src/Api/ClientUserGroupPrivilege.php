<?php

namespace BirdSystem\SDK\Client\Api;

use BirdSystem\SDK\Client\Model\ClientUserGroupPrivilege as ClientUserGroupPrivilegeModel;
use OpenAPI\Runtime\AbstractAPI as AbstractAPI;

class ClientUserGroupPrivilege extends AbstractAPI
{
    /**
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *
     * @return ClientUserGroupPrivilegeModel[]
     */
    public function getCollection(array $queries = []): array
    {
        return $this->client->request('getClientUserGroupPrivilegeCollection', 'get', '/api/client/client_user_group_privileges',
            [
                'query' => $queries,
            ]
        );
    }

    /**
     * @param ClientUserGroupPrivilegeModel $Model The new ClientUserGroupPrivilege
     *                                             resource
     *
     * @return ClientUserGroupPrivilegeModel
     */
    public function postCollection(ClientUserGroupPrivilegeModel $Model): ClientUserGroupPrivilegeModel
    {
        return $this->client->request('postClientUserGroupPrivilegeCollection', 'post', '/api/client/client_user_group_privileges',
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return ClientUserGroupPrivilegeModel
     */
    public function getItem(string $id): ClientUserGroupPrivilegeModel
    {
        return $this->client->request('getClientUserGroupPrivilegeItem', 'get', "/api/client/client_user_group_privileges/{$id}",
            [
            ]
        );
    }

    /**
     * @param string                        $id
     * @param ClientUserGroupPrivilegeModel $Model The updated ClientUserGroupPrivilege
     *                                             resource
     *
     * @return ClientUserGroupPrivilegeModel
     */
    public function putItem(string $id, ClientUserGroupPrivilegeModel $Model): ClientUserGroupPrivilegeModel
    {
        return $this->client->request('putClientUserGroupPrivilegeItem', 'put', "/api/client/client_user_group_privileges/{$id}",
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return mixed
     */
    public function deleteItem(string $id)
    {
        return $this->client->request('deleteClientUserGroupPrivilegeItem', 'delete', "/api/client/client_user_group_privileges/{$id}",
            [
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return ClientUserGroupPrivilegeModel
     */
    public function patchItem(string $id): ClientUserGroupPrivilegeModel
    {
        return $this->client->request('patchClientUserGroupPrivilegeItem', 'patch', "/api/client/client_user_group_privileges/{$id}",
            [
            ]
        );
    }
}
