<?php

namespace BirdSystem\SDK\Client\Api;

use BirdSystem\SDK\Client\Model\ClientVatNumber as ClientVatNumberModel;
use OpenAPI\Runtime\AbstractAPI as AbstractAPI;

class ClientVatNumber extends AbstractAPI
{
    /**
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *
     * @return ClientVatNumberModel[]
     */
    public function getCollection(array $queries = []): array
    {
        return $this->client->request('getClientVatNumberCollection', 'get', '/api/client/client_vat_numbers',
            [
                'query' => $queries,
            ]
        );
    }

    /**
     * @param ClientVatNumberModel $Model The new ClientVatNumber resource
     *
     * @return ClientVatNumberModel
     */
    public function postCollection(ClientVatNumberModel $Model): ClientVatNumberModel
    {
        return $this->client->request('postClientVatNumberCollection', 'post', '/api/client/client_vat_numbers',
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return ClientVatNumberModel
     */
    public function getItem(string $id): ClientVatNumberModel
    {
        return $this->client->request('getClientVatNumberItem', 'get', "/api/client/client_vat_numbers/{$id}",
            [
            ]
        );
    }

    /**
     * @param string               $id
     * @param ClientVatNumberModel $Model The updated ClientVatNumber resource
     *
     * @return ClientVatNumberModel
     */
    public function putItem(string $id, ClientVatNumberModel $Model): ClientVatNumberModel
    {
        return $this->client->request('putClientVatNumberItem', 'put', "/api/client/client_vat_numbers/{$id}",
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return mixed
     */
    public function deleteItem(string $id)
    {
        return $this->client->request('deleteClientVatNumberItem', 'delete', "/api/client/client_vat_numbers/{$id}",
            [
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return ClientVatNumberModel
     */
    public function patchItem(string $id): ClientVatNumberModel
    {
        return $this->client->request('patchClientVatNumberItem', 'patch', "/api/client/client_vat_numbers/{$id}",
            [
            ]
        );
    }
}
