<?php

namespace BirdSystem\SDK\Client\Api;

use BirdSystem\SDK\Client\Model\InvoiceItem as InvoiceItemModel;
use OpenAPI\Runtime\AbstractAPI as AbstractAPI;

class InvoiceItem extends AbstractAPI
{
    /**
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *
     * @return InvoiceItemModel[]
     */
    public function getCollection(array $queries = []): array
    {
        return $this->client->request('getInvoiceItemCollection', 'get', '/api/client/invoice_items',
            [
                'query' => $queries,
            ]
        );
    }

    /**
     * @param InvoiceItemModel $Model The new InvoiceItem resource
     *
     * @return InvoiceItemModel
     */
    public function postCollection(InvoiceItemModel $Model): InvoiceItemModel
    {
        return $this->client->request('postInvoiceItemCollection', 'post', '/api/client/invoice_items',
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return InvoiceItemModel
     */
    public function getItem(string $id): InvoiceItemModel
    {
        return $this->client->request('getInvoiceItemItem', 'get', "/api/client/invoice_items/{$id}",
            [
            ]
        );
    }

    /**
     * @param string           $id
     * @param InvoiceItemModel $Model The updated InvoiceItem resource
     *
     * @return InvoiceItemModel
     */
    public function putItem(string $id, InvoiceItemModel $Model): InvoiceItemModel
    {
        return $this->client->request('putInvoiceItemItem', 'put', "/api/client/invoice_items/{$id}",
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return mixed
     */
    public function deleteItem(string $id)
    {
        return $this->client->request('deleteInvoiceItemItem', 'delete', "/api/client/invoice_items/{$id}",
            [
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return InvoiceItemModel
     */
    public function patchItem(string $id): InvoiceItemModel
    {
        return $this->client->request('patchInvoiceItemItem', 'patch', "/api/client/invoice_items/{$id}",
            [
            ]
        );
    }
}
