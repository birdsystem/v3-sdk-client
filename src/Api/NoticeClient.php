<?php

namespace BirdSystem\SDK\Client\Api;

use BirdSystem\SDK\Client\Model\NoticeClient as NoticeClientModel;
use OpenAPI\Runtime\AbstractAPI as AbstractAPI;

class NoticeClient extends AbstractAPI
{
    /**
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *
     * @return NoticeClientModel[]
     */
    public function getCollection(array $queries = []): array
    {
        return $this->client->request('getNoticeClientCollection', 'get', '/api/client/notice_clients',
            [
                'query' => $queries,
            ]
        );
    }

    /**
     * @param NoticeClientModel $Model The new NoticeClient resource
     *
     * @return NoticeClientModel
     */
    public function postCollection(NoticeClientModel $Model): NoticeClientModel
    {
        return $this->client->request('postNoticeClientCollection', 'post', '/api/client/notice_clients',
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return NoticeClientModel
     */
    public function getItem(string $id): NoticeClientModel
    {
        return $this->client->request('getNoticeClientItem', 'get', "/api/client/notice_clients/{$id}",
            [
            ]
        );
    }

    /**
     * @param string            $id
     * @param NoticeClientModel $Model The updated NoticeClient resource
     *
     * @return NoticeClientModel
     */
    public function putItem(string $id, NoticeClientModel $Model): NoticeClientModel
    {
        return $this->client->request('putNoticeClientItem', 'put', "/api/client/notice_clients/{$id}",
            [
                'json' => $Model->getArrayCopy(),
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return mixed
     */
    public function deleteItem(string $id)
    {
        return $this->client->request('deleteNoticeClientItem', 'delete', "/api/client/notice_clients/{$id}",
            [
            ]
        );
    }

    /**
     * @param string $id
     *
     * @return NoticeClientModel
     */
    public function patchItem(string $id): NoticeClientModel
    {
        return $this->client->request('patchNoticeClientItem', 'patch', "/api/client/notice_clients/{$id}",
            [
            ]
        );
    }
}
