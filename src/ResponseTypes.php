<?php

namespace BirdSystem\SDK\Client;

use OpenAPI\Runtime\ResponseTypes as AbstractResponseTypes;

class ResponseTypes extends AbstractResponseTypes
{
    public static $types = [
        'getAddressBookCollection' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\AddressBook[]',
        ],
        'postAddressBookCollection' => [
            '201.' => 'BirdSystem\\SDK\\Client\\Model\\AddressBook',
        ],
        'getAddressBookItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\AddressBook',
        ],
        'putAddressBookItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\AddressBook',
        ],
        'patchAddressBookItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\AddressBook',
        ],
        'getAmazonAccountCollection' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\AmazonAccount[]',
        ],
        'postAmazonAccountCollection' => [
            '201.' => 'BirdSystem\\SDK\\Client\\Model\\AmazonAccount',
        ],
        'getAmazonAccountItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\AmazonAccount',
        ],
        'putAmazonAccountItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\AmazonAccount',
        ],
        'patchAmazonAccountItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\AmazonAccount',
        ],
        'getClientAdditionalCollection' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientAdditional[]',
        ],
        'postClientAdditionalCollection' => [
            '201.' => 'BirdSystem\\SDK\\Client\\Model\\ClientAdditional',
        ],
        'getClientAdditionalItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientAdditional',
        ],
        'putClientAdditionalItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientAdditional',
        ],
        'patchClientAdditionalItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientAdditional',
        ],
        'getClientCompanyCollection' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientCompany\\AdminReadClientRead[]',
        ],
        'postClientCompanyCollection' => [
            '201.' => 'BirdSystem\\SDK\\Client\\Model\\ClientCompany\\AdminReadClientRead',
        ],
        'getClientCompanyItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientCompany\\AdminReadClientRead',
        ],
        'putClientCompanyItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientCompany\\AdminReadClientRead',
        ],
        'patchClientCompanyItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientCompany\\AdminReadClientRead',
        ],
        'getClientConfigCollection' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientConfig[]',
        ],
        'postClientConfigCollection' => [
            '201.' => 'BirdSystem\\SDK\\Client\\Model\\ClientConfig',
        ],
        'getClientConfigItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientConfig',
        ],
        'putClientConfigItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientConfig',
        ],
        'patchClientConfigItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientConfig',
        ],
        'getClientCountryGuaranteeStatementCollection' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientCountryGuaranteeStatement[]',
        ],
        'postClientCountryGuaranteeStatementCollection' => [
            '201.' => 'BirdSystem\\SDK\\Client\\Model\\ClientCountryGuaranteeStatement',
        ],
        'getClientCountryGuaranteeStatementItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientCountryGuaranteeStatement',
        ],
        'putClientCountryGuaranteeStatementItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientCountryGuaranteeStatement',
        ],
        'patchClientCountryGuaranteeStatementItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientCountryGuaranteeStatement',
        ],
        'getClientCreditLimitTopUpTransactionCollection' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientCreditLimitTopUpTransaction[]',
        ],
        'postClientCreditLimitTopUpTransactionCollection' => [
            '201.' => 'BirdSystem\\SDK\\Client\\Model\\ClientCreditLimitTopUpTransaction',
        ],
        'getClientCreditLimitTopUpTransactionItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientCreditLimitTopUpTransaction',
        ],
        'putClientCreditLimitTopUpTransactionItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientCreditLimitTopUpTransaction',
        ],
        'patchClientCreditLimitTopUpTransactionItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientCreditLimitTopUpTransaction',
        ],
        'getClientGroupDeliveryPackageSizeHandlingFeeCollection' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientGroupDeliveryPackageSizeHandlingFee[]',
        ],
        'postClientGroupDeliveryPackageSizeHandlingFeeCollection' => [
            '201.' => 'BirdSystem\\SDK\\Client\\Model\\ClientGroupDeliveryPackageSizeHandlingFee',
        ],
        'getClientGroupDeliveryPackageSizeHandlingFeeItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientGroupDeliveryPackageSizeHandlingFee',
        ],
        'putClientGroupDeliveryPackageSizeHandlingFeeItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientGroupDeliveryPackageSizeHandlingFee',
        ],
        'patchClientGroupDeliveryPackageSizeHandlingFeeItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientGroupDeliveryPackageSizeHandlingFee',
        ],
        'getClientGroupDeliveryServiceHandlingFeeCollection' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientGroupDeliveryServiceHandlingFee[]',
        ],
        'postClientGroupDeliveryServiceHandlingFeeCollection' => [
            '201.' => 'BirdSystem\\SDK\\Client\\Model\\ClientGroupDeliveryServiceHandlingFee',
        ],
        'getClientGroupDeliveryServiceHandlingFeeItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientGroupDeliveryServiceHandlingFee',
        ],
        'putClientGroupDeliveryServiceHandlingFeeItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientGroupDeliveryServiceHandlingFee',
        ],
        'patchClientGroupDeliveryServiceHandlingFeeItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientGroupDeliveryServiceHandlingFee',
        ],
        'getClientGroupSpontaneousDispatchFeeSettingCollection' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientGroupSpontaneousDispatchFeeSetting[]',
        ],
        'postClientGroupSpontaneousDispatchFeeSettingCollection' => [
            '201.' => 'BirdSystem\\SDK\\Client\\Model\\ClientGroupSpontaneousDispatchFeeSetting',
        ],
        'getClientGroupSpontaneousDispatchFeeSettingItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientGroupSpontaneousDispatchFeeSetting',
        ],
        'putClientGroupSpontaneousDispatchFeeSettingItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientGroupSpontaneousDispatchFeeSetting',
        ],
        'patchClientGroupSpontaneousDispatchFeeSettingItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientGroupSpontaneousDispatchFeeSetting',
        ],
        'getClientGroupCollection' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientGroup[]',
        ],
        'postClientGroupCollection' => [
            '201.' => 'BirdSystem\\SDK\\Client\\Model\\ClientGroup',
        ],
        'getClientGroupItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientGroup',
        ],
        'putClientGroupItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientGroup',
        ],
        'patchClientGroupItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientGroup',
        ],
        'getClientStatementLockCollection' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientStatementLock[]',
        ],
        'postClientStatementLockCollection' => [
            '201.' => 'BirdSystem\\SDK\\Client\\Model\\ClientStatementLock',
        ],
        'getClientStatementLockItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientStatementLock',
        ],
        'putClientStatementLockItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientStatementLock',
        ],
        'patchClientStatementLockItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientStatementLock',
        ],
        'getClientStatementCollection' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientStatement[]',
        ],
        'postClientStatementCollection' => [
            '201.' => 'BirdSystem\\SDK\\Client\\Model\\ClientStatement',
        ],
        'getClientStatementItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientStatement',
        ],
        'putClientStatementItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientStatement',
        ],
        'patchClientStatementItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientStatement',
        ],
        'getClientUserGroupPrivilegeCollection' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientUserGroupPrivilege[]',
        ],
        'postClientUserGroupPrivilegeCollection' => [
            '201.' => 'BirdSystem\\SDK\\Client\\Model\\ClientUserGroupPrivilege',
        ],
        'getClientUserGroupPrivilegeItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientUserGroupPrivilege',
        ],
        'putClientUserGroupPrivilegeItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientUserGroupPrivilege',
        ],
        'patchClientUserGroupPrivilegeItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientUserGroupPrivilege',
        ],
        'getClientUserGroupCollection' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientUserGroup[]',
        ],
        'postClientUserGroupCollection' => [
            '201.' => 'BirdSystem\\SDK\\Client\\Model\\ClientUserGroup',
        ],
        'getClientUserGroupItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientUserGroup',
        ],
        'putClientUserGroupItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientUserGroup',
        ],
        'patchClientUserGroupItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientUserGroup',
        ],
        'getClientUserCollection' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientUser[]',
        ],
        'postClientUserCollection' => [
            '201.' => 'BirdSystem\\SDK\\Client\\Model\\ClientUser',
        ],
        'getClientUserItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientUser',
        ],
        'putClientUserItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientUser',
        ],
        'patchClientUserItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientUser',
        ],
        'getClientVatNumberCollection' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientVatNumber[]',
        ],
        'postClientVatNumberCollection' => [
            '201.' => 'BirdSystem\\SDK\\Client\\Model\\ClientVatNumber',
        ],
        'getClientVatNumberItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientVatNumber',
        ],
        'putClientVatNumberItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientVatNumber',
        ],
        'patchClientVatNumberItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\ClientVatNumber',
        ],
        'getClientCollection' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\Client\\AdminReadClientRead[]',
        ],
        'postClientCollection' => [
            '201.' => 'BirdSystem\\SDK\\Client\\Model\\Client\\AdminReadClientRead',
        ],
        'getClientItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\Client\\AdminReadClientRead',
        ],
        'putClientItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\Client\\AdminReadClientRead',
        ],
        'patchClientItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\Client\\AdminReadClientRead',
        ],
        'getDailyStockStatementCollection' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\DailyStockStatement[]',
        ],
        'postDailyStockStatementCollection' => [
            '201.' => 'BirdSystem\\SDK\\Client\\Model\\DailyStockStatement',
        ],
        'getDailyStockStatementItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\DailyStockStatement',
        ],
        'putDailyStockStatementItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\DailyStockStatement',
        ],
        'patchDailyStockStatementItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\DailyStockStatement',
        ],
        'getDoctrineMigrationVersionsCollection' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\DoctrineMigrationVersions[]',
        ],
        'postDoctrineMigrationVersionsCollection' => [
            '201.' => 'BirdSystem\\SDK\\Client\\Model\\DoctrineMigrationVersions',
        ],
        'getDoctrineMigrationVersionsItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\DoctrineMigrationVersions',
        ],
        'putDoctrineMigrationVersionsItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\DoctrineMigrationVersions',
        ],
        'patchDoctrineMigrationVersionsItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\DoctrineMigrationVersions',
        ],
        'getEbayAccountCollection' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\EbayAccount[]',
        ],
        'postEbayAccountCollection' => [
            '201.' => 'BirdSystem\\SDK\\Client\\Model\\EbayAccount',
        ],
        'getEbayAccountItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\EbayAccount',
        ],
        'putEbayAccountItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\EbayAccount',
        ],
        'patchEbayAccountItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\EbayAccount',
        ],
        'getInvoiceBillingTransactionCollection' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\InvoiceBillingTransaction[]',
        ],
        'postInvoiceBillingTransactionCollection' => [
            '201.' => 'BirdSystem\\SDK\\Client\\Model\\InvoiceBillingTransaction',
        ],
        'getInvoiceBillingTransactionItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\InvoiceBillingTransaction',
        ],
        'putInvoiceBillingTransactionItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\InvoiceBillingTransaction',
        ],
        'patchInvoiceBillingTransactionItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\InvoiceBillingTransaction',
        ],
        'getInvoiceItemCollection' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\InvoiceItem[]',
        ],
        'postInvoiceItemCollection' => [
            '201.' => 'BirdSystem\\SDK\\Client\\Model\\InvoiceItem',
        ],
        'getInvoiceItemItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\InvoiceItem',
        ],
        'putInvoiceItemItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\InvoiceItem',
        ],
        'patchInvoiceItemItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\InvoiceItem',
        ],
        'getInvoiceReferenceCollection' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\InvoiceReference[]',
        ],
        'postInvoiceReferenceCollection' => [
            '201.' => 'BirdSystem\\SDK\\Client\\Model\\InvoiceReference',
        ],
        'getInvoiceReferenceItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\InvoiceReference',
        ],
        'putInvoiceReferenceItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\InvoiceReference',
        ],
        'patchInvoiceReferenceItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\InvoiceReference',
        ],
        'getInvoiceRelatedRecordCollection' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\InvoiceRelatedRecord[]',
        ],
        'postInvoiceRelatedRecordCollection' => [
            '201.' => 'BirdSystem\\SDK\\Client\\Model\\InvoiceRelatedRecord',
        ],
        'getInvoiceRelatedRecordItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\InvoiceRelatedRecord',
        ],
        'putInvoiceRelatedRecordItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\InvoiceRelatedRecord',
        ],
        'patchInvoiceRelatedRecordItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\InvoiceRelatedRecord',
        ],
        'getInvoiceCollection' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\Invoice[]',
        ],
        'postInvoiceCollection' => [
            '201.' => 'BirdSystem\\SDK\\Client\\Model\\Invoice',
        ],
        'getInvoiceItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\Invoice',
        ],
        'putInvoiceItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\Invoice',
        ],
        'patchInvoiceItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\Invoice',
        ],
        'getLicenceAgreementClientConfirmCollection' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\LicenceAgreementClientConfirm[]',
        ],
        'postLicenceAgreementClientConfirmCollection' => [
            '201.' => 'BirdSystem\\SDK\\Client\\Model\\LicenceAgreementClientConfirm',
        ],
        'getLicenceAgreementClientConfirmItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\LicenceAgreementClientConfirm',
        ],
        'putLicenceAgreementClientConfirmItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\LicenceAgreementClientConfirm',
        ],
        'patchLicenceAgreementClientConfirmItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\LicenceAgreementClientConfirm',
        ],
        'getLicenceAgreementCollection' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\LicenceAgreement[]',
        ],
        'postLicenceAgreementCollection' => [
            '201.' => 'BirdSystem\\SDK\\Client\\Model\\LicenceAgreement',
        ],
        'getLicenceAgreementItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\LicenceAgreement',
        ],
        'putLicenceAgreementItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\LicenceAgreement',
        ],
        'patchLicenceAgreementItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\LicenceAgreement',
        ],
        'getNoticeClientConfirmDetailCollection' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\NoticeClientConfirmDetail[]',
        ],
        'postNoticeClientConfirmDetailCollection' => [
            '201.' => 'BirdSystem\\SDK\\Client\\Model\\NoticeClientConfirmDetail',
        ],
        'getNoticeClientConfirmDetailItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\NoticeClientConfirmDetail',
        ],
        'putNoticeClientConfirmDetailItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\NoticeClientConfirmDetail',
        ],
        'patchNoticeClientConfirmDetailItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\NoticeClientConfirmDetail',
        ],
        'getNoticeClientCollection' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\NoticeClient[]',
        ],
        'postNoticeClientCollection' => [
            '201.' => 'BirdSystem\\SDK\\Client\\Model\\NoticeClient',
        ],
        'getNoticeClientItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\NoticeClient',
        ],
        'putNoticeClientItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\NoticeClient',
        ],
        'patchNoticeClientItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\NoticeClient',
        ],
        'getNoticeTargetCollection' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\NoticeTarget[]',
        ],
        'postNoticeTargetCollection' => [
            '201.' => 'BirdSystem\\SDK\\Client\\Model\\NoticeTarget',
        ],
        'getNoticeTargetItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\NoticeTarget',
        ],
        'putNoticeTargetItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\NoticeTarget',
        ],
        'patchNoticeTargetItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\NoticeTarget',
        ],
        'getNoticeCollection' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\Notice[]',
        ],
        'postNoticeCollection' => [
            '201.' => 'BirdSystem\\SDK\\Client\\Model\\Notice',
        ],
        'getNoticeItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\Notice',
        ],
        'putNoticeItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\Notice',
        ],
        'patchNoticeItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\Notice',
        ],
        'getTicketCollection' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\Ticket[]',
        ],
        'postTicketCollection' => [
            '201.' => 'BirdSystem\\SDK\\Client\\Model\\Ticket',
        ],
        'getTicketItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\Ticket',
        ],
        'putTicketItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\Ticket',
        ],
        'patchTicketItem' => [
            '200.' => 'BirdSystem\\SDK\\Client\\Model\\Ticket',
        ],
    ];
}
